/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

/*
function getRequirementsMasterQueryURL(stored_token) {

    var datasourceUrlStart = "https://spreadsheets.google.com/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,toDate(E),F,G,H,I";
    var  whereClause = " WHERE ";
    var  queryEnd = " NOT(B CONTAINS 'Internal Activities') ORDER BY C ASC LABEL toDate(E) 'Open_Date' FORMAT toDate(E) 'dd-mm-yyyy'";
    var datasourceUrlEnd = "&key=1tUI5Gp1AhVASiD_7TeKEFaJXhckmOyj78Ku-TMWvPV4&gid=0&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;
    queryURL = queryURL + queryEnd;

    //console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    //console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}
*/

function getRequirementsMasterDocsQueryURL(stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1tUI5Gp1AhVASiD_7TeKEFaJXhckmOyj78Ku-TMWvPV4/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,toDate(E),F,G,H,I";
    var  whereClause = " WHERE ";
    var  queryEnd = " NOT(B CONTAINS 'Internal Activities') ORDER BY C ASC LABEL toDate(E) 'Open_Date' FORMAT toDate(E) 'dd-mm-yyyy'";
    var datasourceUrlEnd = "&gid=0&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;
    queryURL = queryURL + queryEnd;

    //console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    //console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}