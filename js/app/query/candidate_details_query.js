/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getCandidateInteractionForCandidateIDQueryURL(candidateID,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z " +
        ", AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AX,AY,AZ" +
        ", BA,BB,BC,BD,BE,BF,BG,BH,BI,BJ,BK,BL,BM,BN,BO,BP";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";

    var candidateStartClause = "C CONTAINS '";
    var candidateEndClause = "'";

    var  queryEnd = " (E CONTAINS 'Internally Selected' OR  E CONTAINS 'First Assessment' OR E CONTAINS 'Second Assessment' OR E CONTAINS 'Third Assessment')  LABEL A 'Timestamp', B 'Username', E 'Candidate_Status', G 'Interaction_Comments', W 'Relevant_Skill_Rating', X 'Relevant_Experience_Rating', Y 'Role_Match_Rating',Z 'Notice_Period_Match',AA 'FA_Budget_Match',AB 'Knowledge_level',AC 'Candidate_Job_Eagerness',AD 'Job_Stability_Rating',AE 'Culture_Fit',AF 'Candidate_Showcase_ability',AG 'Hiring_Match' ";

    var datasourceUrlEnd = "&gid=2137357099&access_token="+stored_token;





    var queryURL = columnSelect + whereClause;
    queryURL = queryURL + candidateStartClause + candidateID +  candidateEndClause + andClause;
    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
   // console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}

function getInteractionComments(interactionResults, interactionStatus){

    //var interactionComments = "No Interaction Comments recorded";
    var interactionComments = "Yet to be Assessed";
    var interactionDetails = {
        "Interaction_Comments":interactionComments,
        "Relevant_Skill_Check":"",
        "Relevant_Exp_Check":"",
        "Role_Match_Check":"",
        "Notice_Period_Check":"",
        "Budget_Match_Check":"",
        "Knowledge_level_Check":"",
        "JobStability_Check":"",
        "Job_Eagerness_Check":"",
        "Culture_Fit_Check":"",
        "Candidate_Achievement_Check":"",
        "Hiring_Manager_Check":"",
        "Username":""

    };


if (typeof interactionResults != "undefined") {


    for(var i = 0; i < interactionResults.length; i++)
    {
        var interaction = interactionResults[i];
        var candidateInteractionStatus = interaction.Candidate_Status;
        var candidateInteractionComments = interaction.Interaction_Comments;
        if(interactionStatus == candidateInteractionStatus){
            console.log(interactionStatus);
            console.log("Interaction Comments");
            console.log(candidateInteractionComments);
            if (candidateInteractionComments == null || candidateInteractionComments == "" || candidateInteractionComments == " "){
                interactionDetails.Interaction_Comments = " Assessed but no interaction comments recorded";
            } else {
                interactionDetails.Interaction_Comments = candidateInteractionComments;
            }

            interactionDetails.Relevant_Skill_Check=interaction.Relevant_Skill_Rating;
            interactionDetails.Relevant_Exp_Check=interaction.Relevant_Experience_Rating;
            interactionDetails.Role_Match_Check=interaction.Role_Match_Rating;
            interactionDetails.Notice_Period_Check=interaction.Notice_Period_Match;
            interactionDetails.Budget_Match_Check=interaction.FA_Budget_Match;
            interactionDetails.Knowledge_level_Check=interaction.Knowledge_level;
            interactionDetails.JobStability_Check=interaction.Job_Stability_Rating;
            interactionDetails.Job_Eagerness_Check=interaction.Candidate_Job_Eagerness;
            interactionDetails.Culture_Fit_Check=interaction.Culture_Fit;
            interactionDetails.Candidate_Achievement_Check=interaction.Candidate_Showcase_ability;
            interactionDetails.Hiring_Manager_Check=interaction.Hiring_Match;
            interactionDetails.Username = interaction.Username;
        }
    }
}
    return interactionDetails;
}

function getCandidatesStatusForRequirement(requirementID,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/18z3R8Ot1V9CKo12CuqrcArKW-cLlRbr5vsiBQFNJG68/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O ";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";

    var requirementStartClause = "A = '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";
    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause ;
    }

    var  queryEnd = " LABEL A 'Requirement_ID', B 'CV_SENT_CLIENT', C 'CV_REJECTED', D 'INTERVIEW_FAILED', E 'CV_CREATED', F 'OFFER_REJECTED'" +
        " , G 'OFFER_ACCEPTED',  H 'Active_CV',I 'INTERNALLY_SELECTED', J 'INTERVIEW_SCHEDULED', K 'FIRST_ASSESSMENT_PASSED', L 'SECOND_ASSESSMENT_PASSED'," +
        " M 'THIRD_ASSESSMENT_PASSED', N 'JOINED', O 'CV_INTERNAL_CONTACT' ";

    var datasourceUrlEnd = "&gid=1441113324&access_token="+stored_token;


    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    // console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}