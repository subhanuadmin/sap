/**
 * Created by snehita on 12/18/2015.
 */


    function getMeetingReportURL(userName, fromDate, toDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1LukDxWuWJ-4rmq6rNl5URTv8pYeEnPp00FPyDom43dA/gviz/tq?tq=";
        var  columnSelect = "SELECT A,B,C,D,E,F ";
        var  whereClause = " WHERE ";
        var  andClause = " AND ";
        var  queryEnd = "  NOT(B CONTAINS 'Username') ORDER BY C DESC LABEL A 'Timestamp', B 'Username', C 'Meeting_Date', D 'Meeting_Type', E 'Attendees', F 'Meeting_Agenda'";
        //var datasourceUrlEnd = "&key=1-kaFkR2d96uutTzRvSTbLwuRd6fD5b15M7-K23OwFkQ&gid=1106745654";
    var datasourceUrlEnd = "&gid=174632617&access_token="+stored_token;

        var userStartClause = " B CONTAINS '";
        var userEndClause = "'";

        var fromDateStartClause = "toDate(C) >= date '";
        // var fromDate = "" ;
        var fromDateEndClause = "'";

        var toDateStartClause = "toDate(C) <= date '";
        // var toDate = "" ;
        var toDateEndClause = "'";

        var queryURL = columnSelect + whereClause;

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause ;
    }

        if (fromDate != "" ){
            console.log('From Date Selected');
            queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
        }

        if (toDate != "" ){
            console.log(' To Date Selected');
            queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
        }

        queryURL = queryURL + queryEnd;

        console.log(queryURL);
        var encodedQueryURI = encodeURIComponent(queryURL);
        console.log(encodedQueryURI);
        var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
        // console.log(datasourceUrl);
        return datasourceUrl;
    }

