/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getDocumentsQueryURL(requirementID,statusID, userName,fromDate,toDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1pAP5Ypbznk-ALcxv--E4z2X3Eg6CEdnRd2AfUwJCKC8/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ,BA,BB,BC,BD,BE,BF,BG,BH,BI,BJ,BK,BL,BM,BN,BO,BP,BQ,BR,BS,BT,BU,BV";
    var  whereClause = " WHERE ";
    var requirementStartClause = "I CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var  andClause = " AND ";
    var statusStartClause = "N CONTAINS '";
   // var  statusID = "" ;
    var statusEndClause = "'";

    var userStartClause = "C Contains '";
    //var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(A) >= date '";
   // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(A) <= date '";
   // var toDate = "" ;
    var toDateEndClause = "'";

    var  queryEnd = " NOT(I CONTAINS 'Test')  ORDER BY D,C ";
    var datasourceUrlEnd = "&gid=1446476273&access_token="+stored_token;
    var query;


    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
       // console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }

    if (statusID != 'All' ){
        //console.log('Status Condition');
        queryURL = queryURL + statusStartClause + statusID +  statusEndClause + andClause;
    }

    if (userName != 'All' ){
      //  console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    if (fromDate != "" ){
       // console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        //console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }
    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}