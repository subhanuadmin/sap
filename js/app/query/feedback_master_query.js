/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getFeedbackMasterQueryURL(addedByName,feedbackname, startDate, endDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/15pvIDW8ivr5Y2WBwc3JTCPExqIlk9sbgVTEwfqVHsyU/gviz/tq?tq=";
    //var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I";
    var  columnSelect = "SELECT I,H,J,E";
    //var  columnSelect = "SELECT A";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var  queryEnd = " E != ' ' ORDER BY A DESC";
    var datasourceUrlEnd = "&gid=1709276304&access_token="+stored_token;




    var userStartClause = "D CONTAINS '";
    var userEndClause = "'";

    var queryURL = columnSelect + whereClause;

    if (feedbackname != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + feedbackname +  userEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}