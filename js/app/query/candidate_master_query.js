/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getCandidatesMasterQueryURL(requirementID,userName,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE ";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var requirementStartClause = "D CONTAINS '";
    var requirementEndClause = "'";
    var userStartClause = "C = '";
    //var  userName = "" ;
    var userEndClause = "'";
    var  queryEnd = " NOT(E CONTAINS 'Duplicate') ORDER BY A DESC  LABEL A 'Timestamp', B 'Candidate_ID', C 'UserName', D 'Requirement_ID', E 'Name', F 'Gender', G 'DOB', H 'Qualification', I 'Experience', J 'Contact_Number', K 'Alternate_Number', L 'Email_ID', M 'Current_Location',  N 'Company', O 'Current_Desgination', P 'Current_Salary', Q 'Expected_Salary', R 'Language_1', S 'Language_2', T 'Resignation_Status', U 'Time_To_Join' , V 'Resume_Source_Date', W 'Resume_Link', X 'Technology', Y 'Communication', Z 'Candidate_Rating', AA 'Target_Requirement',  AB 'Candidate_Source', AC 'Candidate_Contact_Mode', AD 'social_media', AE 'Time_Spent'";
    //var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=194951059";
    var datasourceUrlEnd = "&gid=1215267491&access_token="+stored_token;
    var query;
    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }


    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}

function getCandidateSummaryQueryURL(candidateID,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE ";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var candidateStartClause = "B = '";
//    var  requirementID = "" ;
    var candidateEndClause = "'";

    var  queryEnd = " NOT(E CONTAINS 'Duplicate') LABEL A 'Timestamp', B 'Candidate_ID', C 'UserName', D 'Requirement_ID', E 'Name', F 'Gender', G 'DOB', H 'Qualification', I 'Experience', J 'Contact_Number', K 'Alternate_Number', L 'Email_ID', M 'Current_Location',  N 'Company', O 'Current_Desgination', P 'Current_Salary', Q 'Expected_Salary', R 'Language_1', S 'Language_2', T 'Resignation_Status', U 'Time_To_Join' , V 'Resume_Source_Date', W 'Resume_Link', X 'Technology', Y 'Communication', Z 'Candidate_Rating', AA 'Target_Requirement',  AB 'Candidate_Source', AC 'Candidate_Contact_Mode', AD 'social_media', AE 'Time_Spent'";
    //var datasourceUrlEnd = "&key=1PPULK_aLIGQKZkFn12mEXVMruWQCIqaFV9Atrm9aM0E&gid=194951059";
    //var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=194951059";
    var datasourceUrlEnd = "&gid=1215267491&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;
        queryURL = queryURL + candidateStartClause + candidateID +  candidateEndClause + andClause;
        queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}