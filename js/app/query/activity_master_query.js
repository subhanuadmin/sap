/**
 * Created by snehita on 4/9/2016.
 */

function getActivityReportURL(fromDate, toDate,userName,stored_token) {
    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1pAP5Ypbznk-ALcxv--E4z2X3Eg6CEdnRd2AfUwJCKC8/gviz/tq?tq=";
    var  columnSelect = "SELECT A,C,D,I,J,BB";
    var  whereClause = " WHERE ";
    var andClause= " AND ";
    var  queryEnd = " NOT(C CONTAINS 'Username') ORDER BY A DESC LABEL A 'Timestamp', C 'Username', D 'Activity_Category', I 'Requirement_ID', J 'Time_Spent', BB 'Activity_Comments'";
    var datasourceUrlEnd = "&gid=1078186497&access_token="+stored_token;


    var query;

    var fromDateStartClause = "toDate(A) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(A) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var userStartClause = " C CONTAINS '";
    var userEndClause = "'";

    var queryURL = columnSelect + whereClause;

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log('To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }


    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}

