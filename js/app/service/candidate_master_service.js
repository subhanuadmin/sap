/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

angular.module('CandidateMasterServices', [],function($provide){
    $provide.factory('CandidateMasterQueryService', function($http, $log){
        $log.log("Candidate Service....");
        return {
            getAllCandidates: function() {
                $log.log("getAllCandidates....");
                var masterListURL = "https://script.google.com/a/macros/subhanu.com/s/AKfycbxADPZw-9MEmlE_SZhOt9DJQHvvt61zFBe-iNtCSK-5ltSfBvh_/exec?id=18z3R8Ot1V9CKo12CuqrcArKW-cLlRbr5vsiBQFNJG68&sheet=CandidatesMaster&callback=JSON_CALLBACK";
                $log.log(masterListURL);
                return $http.jsonp(masterListURL).
                    then(function(response) {
                        console.log("Inside CandidateService .. Status" + response.status);
                        /*  if (response.status == 200){
                         //console.log("Inside CandidateService.. " + JSON.stringify(response.data));
                         //console.log("Inside CandidateService.. " + JSON.stringify(response.data));
                         }*/
                        return response;
                    });
            }
        };
    });

});