/**
 * Created by snehita on 1/11/2016.
 */
angular.module('MeetingMasterServices', [],function($provide){
    $provide.factory('MeetingMasterQueryService', function($http, $log){
        $log.log("CandidateMasterQueryService....");
        return {
            meetings: function() {
                $log.log("getAllCandidates....");
                var masterListURL = "https://script.google.com/a/macros/subhanu.com/s/AKfycbxADPZw-9MEmlE_SZhOt9DJQHvvt61zFBe-iNtCSK-5ltSfBvh_/exec?id=18z3R8Ot1V9CKo12CuqrcArKW-cLlRbr5vsiBQFNJG68&sheet=CandidatesMaster&callback=JSON_CALLBACK";
                $log.log(masterListURL);
                return $http.jsonp(masterListURL).
                    then(function(response) {
                        console.log("Inside CandidateService .. Status" + response.status);
                        /*  if (response.status == 200){
                         //console.log("Inside CandidateService.. " + JSON.stringify(response.data));
                         //console.log("Inside CandidateService.. " + JSON.stringify(response.data));
                         }*/
                        return response;
                    });
            }
        };
    });

});