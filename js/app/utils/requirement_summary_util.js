/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getRequirementSummaryCount(activities) {

    var activitySummaryCount = {
        "Requirement_Capture":
        {
            "Time_Spent": 0
        },
        "Execution_Planning":
        {
            "Time_Spent": 0
        },
        "Create_Workaids":
        {
            "Time_Spent": 0
        },
        "Manage_Posting":
        {
            "Time_Spent": 0
        },
        "Identify_Sources":
        {
            "Total_Job_Boards_Posted": 0,
            "Job_Boards_List_URL":'',
            "Time_Spent": 0
        },
        "Build_Pool":
        {
            "Job_Boards_From": '',
            "Pool_Name": '',
            "Pool_Size": '',
            "Total_Pool_Size": 0,
            "Time_Spent": 0
        },
        "Build_Pool_Unsubscribed":
        {
            "Total_Call_References": 0,
            "Total_Direct_Mail_References": 0,
            "Total_Messages_Received": 0,
            "Total_Whatsapp_Received": 0,
            "Total_Mail_Responses_Received": 0,
            "Total_Missed_Calls": 0,
            "Total_Received_Calls": 0,
            "Total_Needs_Subhanu_Followup": 0,
            "Total_Automatch": 0,
            "Total_Internal_DB": 0,
            "Time_Spent": 0
        },
        "Candidate_Assessment":
        {
            "Pool_Name": '',
            "Total_Num_Of_Cold_calls": 0,
            "Total_Num_Of_Recvd_Calls": 0,
            "Total_Num_Of_RNR": 0,
            "Total_Num_Of_Not_Relevant": 0,
            "Total_Num_Of_Not_Reachable": 0,
            "Total_Needs_Subhanu_Followup": 0,
            "Total_Num_Of_First_Assessments_Done": 0,
            "Total_Num_Of_First_Assessments_Failed": 0,
            "Total_Num_Of_First_Assessments_Passed": 0,
            "Time_Spent": 0
        },
        "Publish_Posting":
        {
            "Num_Of_Job_Boards_Published": 0,
            "Name_Job_Boards_Published": '',
            "Time_Spent": 0
        },
        "Resume_Shortlisting":
        {
            "Pool_Name": '',
            "Total_Num_Of_Resumes_Assessed": 0,
            "Total_Num_Of_Resumes_Shortlisted": 0,
            "Total_Num_Of_Resumes_Pending": 0,
            "Time_Spent": 0
        },
        "Response_Processing":
        {
            "Total_Num_Of_Responses_Received": 0,
            "Total_Num_Of_Responses_Processed": 0,
            "Total_Num_Of_Responses_Pending": 0,
            "Time_Spent": 0
        },
        "Candidate_Reach_Out":
        {
            "Total_Num_Of_Target_Mails": 0,
            "Total_Num_Of_Direct_Mails": 0,
            "Total_Num_Of_Direct_Mails_ID": 0,
            "Total_Num_Of_Sms_sent": 0,
            "Total_Num_Of_Whatsapp_sent": 0,
            "Total_Num_Of_linkedin_sent": 0,
            "Time_Spent": 0
         },
        "Subhanu_Growth":
        {
            "Total_Num_Of_Internal_DB": 0,
            "Total_Num_Of_FB_Added": 0,
            "Time_Spent": 0
        },
        "Requirement_Summary":{
            "Total_Expended_Effort": 0
        }
    };

    if (typeof activities == "undefined"){
        console.log("Reset All Counts");
     } else {
        for (var i = 0; i < activities.length; i++) {
            var activity = activities[i];
            activitySummaryCount.Requirement_Summary.Total_Expended_Effort = activitySummaryCount.Requirement_Summary.Total_Expended_Effort + Number(activity.Time_Spent);
            //console.log(activity.Activity_Category);
            if (activity.Activity_Category == 'Requirement Capture') {
                activitySummaryCount.Requirement_Capture.Time_Spent = activitySummaryCount.Requirement_Capture.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Execution Planning') {
                activitySummaryCount.Execution_Planning.Time_Spent = activitySummaryCount.Execution_Planning.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Manage Posting') {
                activitySummaryCount.Manage_Posting.Time_Spent = activitySummaryCount.Manage_Posting.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Create Workaids') {
                activitySummaryCount.Create_Workaids.Time_Spent = activitySummaryCount.Create_Workaids.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Identify Sources') {
                activitySummaryCount.Identify_Sources.Total_Job_Boards_Posted = activitySummaryCount.Identify_Sources.Total_Job_Boards_Posted + Number(activity.Job_Boards_Identified);
                activitySummaryCount.Identify_Sources.Job_Boards_List_URL = activity.Doc_URL;
                activitySummaryCount.Identify_Sources.Time_Spent = activitySummaryCount.Identify_Sources.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Publish Posting') {
                if (activitySummaryCount.Publish_Posting.Name_Job_Boards_Published == '') {
                    activitySummaryCount.Publish_Posting.Name_Job_Boards_Published = activity.Job_Board_Published;
                } else {
                    activitySummaryCount.Publish_Posting.Name_Job_Boards_Published = activitySummaryCount.Publish_Posting.Name_Job_Boards_Published + ',' + activity.Job_Board_Published;
                }

                activitySummaryCount.Publish_Posting.Num_Of_Job_Boards_Published = Number(activitySummaryCount.Publish_Posting.Num_Of_Job_Boards_Published) + 1;
                activitySummaryCount.Publish_Posting.Time_Spent = activitySummaryCount.Publish_Posting.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Build Pool' && activity.Build_Pool_Type == 'Pool from Subscribed Sources' ) {
                //console.log(activity.Pool_Size);
                if (activitySummaryCount.Build_Pool.Job_Boards_From == '') {
                    activitySummaryCount.Build_Pool.Job_Boards_From = activity.Pool_from_Jobboard;
                } else {
                    activitySummaryCount.Build_Pool.Job_Boards_From = activitySummaryCount.Build_Pool.Job_Boards_From + ', ' + activity.Pool_from_Jobboard;
                }
                if (activitySummaryCount.Build_Pool.Pool_Name == '') {
                    activitySummaryCount.Build_Pool.Pool_Name = activity.Pool_Identifier;
                } else {
                    activitySummaryCount.Build_Pool.Pool_Name = activitySummaryCount.Build_Pool.Pool_Name + ', ' + activity.Pool_Identifier;
                }
                if (activitySummaryCount.Build_Pool.Pool_Size == '') {
                    activitySummaryCount.Build_Pool.Pool_Size = activity.Pool_Size;
                } else {
                    activitySummaryCount.Build_Pool.Pool_Size = activitySummaryCount.Build_Pool.Pool_Size + ', ' + activity.Pool_Size;
                }
                activitySummaryCount.Build_Pool.Total_Pool_Size = activitySummaryCount.Build_Pool.Total_Pool_Size + activity.Pool_Size;
                activitySummaryCount.Build_Pool.Time_Spent = activitySummaryCount.Build_Pool.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Build Pool' && activity.Build_Pool_Type == 'Pool from Unsubscribed Sources') {
                //console.log(activity.Pool_Size);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Call_References = activitySummaryCount.Build_Pool_Unsubscribed.Total_Call_References + Number(activity.Ref_Collect_Count);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Direct_Mail_References = activitySummaryCount.Build_Pool_Unsubscribed.Total_Direct_Mail_References + Number(activity.DM_References);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Messages_Received = activitySummaryCount.Build_Pool_Unsubscribed.Total_Messages_Received + Number(activity.Msgs_Recvd);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Whatsapp_Received = activitySummaryCount.Build_Pool_Unsubscribed.Total_Whatsapp_Received + Number(activity.Whatsapp_Recvd);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Mail_Responses_Received = activitySummaryCount.Build_Pool_Unsubscribed.Total_Mail_Responses_Received + Number(activity.Mail_Respns_Recvd);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Missed_Calls = activitySummaryCount.Build_Pool_Unsubscribed.Total_Missed_Calls + Number(activity.Missed_Calls);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Received_Calls = activitySummaryCount.Build_Pool_Unsubscribed.Total_Received_Calls + Number(activity.Recvd_Call_Count);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Needs_Subhanu_Followup = activitySummaryCount.Build_Pool_Unsubscribed.Total_Needs_Subhanu_Followup + Number(activity.NSF_Pending);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Automatch = activitySummaryCount.Build_Pool_Unsubscribed.Total_Automatch + Number(activity.Automatch_Count);
                activitySummaryCount.Build_Pool_Unsubscribed.Total_Internal_DB = activitySummaryCount.Build_Pool_Unsubscribed.Total_Internal_DB + Number(activity.InternalDB_Pool);
                activitySummaryCount.Build_Pool_Unsubscribed.Time_Spent = activitySummaryCount.Build_Pool_Unsubscribed.Time_Spent + Number(activity.Time_Spent);
            }

            if (activity.Activity_Category == 'Candidate Reach Out') {
                console.log("activity.LinkedIN_Messages -> " + activity.LinkedIN_Messages);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Target_Mails = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Target_Mails + Number(activity.Target_Mails);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Direct_Mails = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Direct_Mails + Number(activity.Direct_Mails);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Direct_Mails_ID = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Direct_Mails_ID + Number(activity.DM_ID_Sent);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Sms_sent = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Sms_sent + Number(activity.Sms_Sent);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Whatsapp_sent = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_Whatsapp_sent + Number(activity.Whatsapp_Count);
                activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_linkedin_sent = activitySummaryCount.Candidate_Reach_Out.Total_Num_Of_linkedin_sent + Number(activity.LinkedIN_Messages);
                activitySummaryCount.Candidate_Reach_Out.Time_Spent = activitySummaryCount.Candidate_Reach_Out.Time_Spent + Number(activity.Time_Spent);

            }
            if (activity.Activity_Category == 'Candidate Assessment') {
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_Cold_calls = activitySummaryCount.Candidate_Assessment.Total_Num_Of_Cold_calls + Number(activity.Cold_Calls);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_Recvd_Calls = activitySummaryCount.Candidate_Assessment.Total_Num_Of_Recvd_Calls + Number(activity.Recvd_Calls_Assessment);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_RNR = activitySummaryCount.Candidate_Assessment.Total_Num_Of_RNR + Number(activity.RNP_Count);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_Not_Relevant = activitySummaryCount.Candidate_Assessment.Total_Num_Of_Not_Relevant + Number(activity.Not_Relevant);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_Not_Reachable = activitySummaryCount.Candidate_Assessment.Total_Num_Of_Not_Reachable + Number(activity.Not_Reachable);
                activitySummaryCount.Candidate_Assessment.Total_Needs_Subhanu_Followup = activitySummaryCount.Candidate_Assessment.Total_Needs_Subhanu_Followup + Number(activity.Assessment_NSF_Count);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Done = activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Done + Number(activity.FA_Completed);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Failed = activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Failed + Number(activity.FA_Completed);
                activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Passed = activitySummaryCount.Candidate_Assessment.Total_Num_Of_First_Assessments_Passed + Number(activity.FA_Passed);
                activitySummaryCount.Candidate_Assessment.Time_Spent = activitySummaryCount.Candidate_Assessment.Time_Spent + Number(activity.Time_Spent);

            }
            if (activity.Activity_Category == 'Resume Shortlisting') {
                if (activitySummaryCount.Resume_Shortlisting.Pool_Name == '') {
                    activitySummaryCount.Resume_Shortlisting.Pool_Name = activity.Pool_Name;
                } else {
                    activitySummaryCount.Resume_Shortlisting.Pool_Name = activitySummaryCount.Resume_Shortlisting.Pool_Name + ', ' + activity.Pool_Name;
                }
                activitySummaryCount.Resume_Shortlisting.Total_Num_Of_Resumes_Assessed = activitySummaryCount.Resume_Shortlisting.Total_Num_Of_Resumes_Assessed + Number(activity.Resumes_Assessed);
                activitySummaryCount.Resume_Shortlisting.Total_Num_Of_Resumes_Shortlisted = activitySummaryCount.Resume_Shortlisting.Total_Num_Of_Resumes_Shortlisted + Number(activity.Resumes_Shortlisted);
                activitySummaryCount.Resume_Shortlisting.Time_Spent = activitySummaryCount.Resume_Shortlisting.Time_Spent + Number(activity.Time_Spent);
            }
            if (activity.Activity_Category == 'Response Processing') {
                activitySummaryCount.Response_Processing.Total_Num_Of_Responses_Processed = activitySummaryCount.Response_Processing.Total_Num_Of_Responses_Processed + Number(activity.Responses_Processed);
                activitySummaryCount.Response_Processing.Total_Num_Of_Responses_Pending = activitySummaryCount.Response_Processing.Total_Num_Of_Responses_Pending + Number(activity.Responses_Pending);
                activitySummaryCount.Response_Processing.Time_Spent = activitySummaryCount.Response_Processing.Time_Spent + Number(activity.Time_Spent);
            }

        }
    }
    return activitySummaryCount;
}

function getCandidateStatusSummary(interactions){

    var candidateSummaries = {};

    var candidateStatusSummary = {
        "Total_Expended_Effort":0,
        candidateStatusSummaries:candidateSummaries
    };


    var addedSummary = "";
    //console.log("Total Interactions -> " + interactions.length);
    for(var i = 0; i < interactions.length; i++) {
        var interaction = interactions[i];
        //console.log("----------------------------------------------------------------------------------------------------------");
        //console.log("Iteration -> " + i);
        //console.log("Current Status -> " + interaction.Candidate_Status);
        candidateStatusSummary.Total_Expended_Effort = candidateStatusSummary.Total_Expended_Effort + Number(interaction.Time_Spent);
        var currentStatus = interaction.Candidate_Status.replace(/\s+/g,"_");
        currentStatus = currentStatus.replace("(","");
        currentStatus = currentStatus.replace(")","");
        currentStatus = currentStatus.replace("-","");
        //console.log("Modified Current Status -> " + currentStatus);
        if (currentStatus in candidateSummaries)
        {
            //console.log("Already Present Status -> " + currentStatus);
            // modify key here
            addedSummary = candidateSummaries[currentStatus];
            //console.log("Already Present Count -> " + addedSummary.Count);
            //console.log("Already Present Effort -> " + addedSummary.Effort);
            //console.log("To be Added -> " + interaction.Time_Spent);
            interaction.modified_status = currentStatus;
            addedSummary.InteractionStatus = currentStatus;
            addedSummary.Count = Number(addedSummary.Count) + 1;
            addedSummary.Effort = Number(addedSummary.Effort) + Number(interaction.Time_Spent);
            //console.log("After Adding Count -> " + addedSummary.Count);
            //console.log("After Adding Effort -> " + addedSummary.Effort);

        }else {
            //console.log("New -> " + currentStatus);
            //console.log("New -> Time spent " + interaction.Time_Spent);
            addedSummary = new StatusSummary(currentStatus,1,Number(interaction.Time_Spent));
            candidateSummaries[currentStatus]= addedSummary ;
            interaction.modified_status = currentStatus;
            //console.log("New  Count -> " + addedSummary.Count);
            //console.log("New  Effort -> " + addedSummary.Effort);
            //candidateSummaries[i]= addedSummary ;
        }

    }

/*    var secondOnHold = getFilteredResultsForStatus(interactions, "Second_Assessment_OnHold_Internal");
   //console.log("secondOnHold Count " + secondOnHold.length);
    if (typeof candidateStatusSummary.candidateStatusSummaries.Second_Assessment_OnHold_Internal.Count == "undefined" ){
        console.log("No Second Assessments Found");
    } else {
        candidateStatusSummary.candidateStatusSummaries.Second_Assessment_OnHold_Internal.Count = secondOnHold.length;
    }


    var thirdOnHold = getFilteredResultsForStatus(interactions, "Third_Assessment_OnHold_Internal");
    if (typeof candidateStatusSummary.candidateStatusSummaries.Third_Assessment_OnHold_Internal.Count == "undefined" ){
        console.log("No Third Assessments Found");
    } else {
        candidateStatusSummary.candidateStatusSummaries.Third_Assessment_OnHold_Internal.Count = thirdOnHold.length;
    }*/

    //var candidateData = JSON.stringify(candidateSummaries);
    var candidateData = JSON.stringify(candidateStatusSummary);
    //console.log("After Stringify");
    //console.log(JSON.stringify(candidateData));
    var candidateJSObject = JSON.parse(candidateData);
    //console.log("After parse");
    //console.log(JSON.stringify(candidateJSObject));

    return candidateJSObject;
}

function StatusSummary(candidateStatus, statusCount, timeSpent){
    //console.log("statusSummary");
    this.InteractionStatus = candidateStatus;
    this.Count = statusCount;
    this.Effort = timeSpent;
}

function getFilteredResultsForStatus(interactions, interactionStatus){
    //console.log("interactionStatus -> " + interactionStatus);

    var filteredArray = new Array();
    var activeCVs = {};
    var rejectedCandidates = {};
    var firstAssessmentPassed = {};
    var secondAssessmentCompleted = {};
    var secondAssessmentNotOnHold = {};
    var secondAssessmentOnHold = {};
    var thirdAssessmentNotOnHold = {};
    var thirdAssessmentOnHold = {};
    var secondAssessmentPassed = {};
    var thirdAssessmentCompleted = {};
    var thirdAssessmentPassed = {};
    var InternalSelectionCompleted = {};
    var cvSentInternal = {};
    var cvSentToClient = {};


    //console.log("Total Interactions -> " + interactions.length);
    for(var i = 0; i < interactions.length; i++) {
        var interaction = interactions[i];
        //console.log("Iteration -> " + i);
        //console.log("Candidate ID -> " + interaction.Candidate_ID);
        //console.log("interaction.modified_status -> " + interaction.modified_status);
        if (interaction.modified_status == interactionStatus
            && interactionStatus != 'Active_CV'
            && interactionStatus != 'Second_Assessment_OnHold_Internal'
            && interactionStatus != 'Third_Assessment_OnHold_Internal')
        {
            console.log("Match Found");
            filteredArray.push(interaction);
        }

        if(interactionStatus == 'Active_CV' && interaction.modified_status == 'CV_Sent_to_Client' ){
            //console.log("Active CV Candidate Match Found");
            activeCVs[interaction.Candidate_ID] = interaction;
        }

        if(interactionStatus == 'Active_CV' && (interaction.modified_status == 'Client_Interview_Failed' ||
                                                interaction.modified_status == 'Offer_Accepted' ||
                                                interaction.modified_status == 'Revised_Offer_Accepted' ||
                                                interaction.modified_status == 'Offer_Accepted_Not_Joining' ||
                                                interaction.modified_status == 'Offer_Rejected' ||
                                                interaction.modified_status == 'CV_Rejected_by_Client' ||
                                                interaction.modified_status == 'CV_Sent_To_Client_Candidate_Dropped' ||
                                                interaction.modified_status == 'Revised_Offer_Rejected') ){
            //console.log("Rejected Match Found");
            rejectedCandidates[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'First_Assessment_Passed_Internal'){
            firstAssessmentPassed[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Second_Assessment_Passed_Internal' ||
            interaction.modified_status == 'Second_Assessment_Failed_Internal' ||
            interaction.modified_status == 'Second_Assessment_OnHold_Internal' ||
            interaction.modified_status == 'Second_Assessment_PassedInternal_Candidate_Dropped') {
            //console.log("Second Assessment Completed");
            secondAssessmentCompleted[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Second_Assessment_OnHold_Internal') {
            //console.log("Second Assessment On Hold" +  interaction.Candidate_ID);
            secondAssessmentOnHold[interaction.Candidate_ID] = interaction;
        }

        if(interaction.modified_status == 'Second_Assessment_Passed_Internal' ||
            interaction.modified_status == 'Second_Assessment_Failed_Internal' ||
            interaction.modified_status == 'Second_Assessment_PassedInternal_Candidate_Dropped') {
            //console.log("Second Assessment Not On Hold " + interaction.Candidate_ID);
            secondAssessmentNotOnHold[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Second_Assessment_Passed_Internal'){
            secondAssessmentPassed[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Third_Assessment_Passed_Internal' ||
            interaction.modified_status == 'Third_Assessment_Failed_Internal' ||
            interaction.modified_status == 'Third_Assessment_OnHold_Internal' ||
            interaction.modified_status == 'Third_Assessment_PassedInternal_Candidate_Dropped') {
            //console.log("Second Assessment Completed");
            thirdAssessmentCompleted[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Third_Assessment_Passed_Internal' ||
            interaction.modified_status == 'Third_Assessment_Failed_Internal' ||
            interaction.modified_status == 'Third_Assessment_PassedInternal_Candidate_Dropped') {
            //console.log("Second Assessment Completed");
            thirdAssessmentNotOnHold[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Third_Assessment_Passed_Internal'){
            thirdAssessmentPassed[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Third_Assessment_OnHold_Internal') {
            //console.log("Second Assessment Completed");
            thirdAssessmentOnHold[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'Internally_Selected__CV_to_be_Created') {
            //console.log("Internally_Selected__CV_to_be_Created");
            InternalSelectionCompleted[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'CV_Sent_to_Internal_Contact'){
            cvSentInternal[interaction.Candidate_ID] = interaction;
        }
        if(interaction.modified_status == 'CV_Sent_to_Client') {
            //console.log("Second Assessment Completed");
            cvSentToClient[interaction.Candidate_ID] = interaction;
        }
    }

    //console.log(JSON.stringify(filteredArray));
   // console.log("Completed Status Analysis");
    if(interactionStatus == 'Active_CV') {
        //console.log("No of Active CV's" + activeCVs.length);
        //console.log("No of rejectedCandidates " + rejectedCandidates.length);
        //console.log("Starting Active CV Iteration");
        var  activeCVCount = 0;
        for (var activeInteraction in activeCVs)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(activeInteraction in rejectedCandidates){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(activeCVs[activeInteraction]);
            }

        }
    }
    if(interactionStatus == 'Second_Assessment_Pending') {
        //console.log("Second_Assessment_Pending");
        var  activeCVCount = 0;
        for (var firstAssessment in firstAssessmentPassed)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(firstAssessment in secondAssessmentCompleted){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(firstAssessmentPassed[firstAssessment]);
            }

        }
    }
    if(interactionStatus == 'Third_Assessment_Pending') {
        //console.log("Third_Assessment_Pending");
        var  activeCVCount = 0;
        for (var secondAssessment in secondAssessmentPassed)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(secondAssessment in thirdAssessmentCompleted){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(secondAssessmentPassed[secondAssessment]);
            }

        }
    }

    if(interactionStatus == 'Internally_Selected_Pending_Non_CSE') {
        //console.log("Internally_Selected_Pending");
        var  activeCVCount = 0;
        for (var thirdAssessment in thirdAssessmentPassed)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(thirdAssessment in InternalSelectionCompleted){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(thirdAssessmentPassed[thirdAssessment]);
            }

        }
    }
    if(interactionStatus == 'Internally_Selected_Pending_CSE') {
        //console.log("Internally_Selected_Pending CSE");
        var  activeCVCount = 0;
        for (var secondAssessment in secondAssessmentPassed)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(secondAssessment in InternalSelectionCompleted){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(secondAssessmentPassed[secondAssessment]);
            }

        }
    }
    if(interactionStatus == 'CV_Send_Pending') {
        //console.log("CV_Send_Pending");
        var  activeCVCount = 0;
        for (var internalCV in InternalSelectionCompleted)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(internalCV in cvSentToClient){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(InternalSelectionCompleted[internalCV]);
            }

        }
    }

    if(interactionStatus == 'Second_Assessment_OnHold_Internal') {
        //console.log("CV_Send_Pending");
        var  activeCVCount = 0;
        for (var secondOnHold in secondAssessmentOnHold)
        {
            //console.log(activeCVCount++);
            if(secondOnHold in secondAssessmentNotOnHold){
                console.log("Second Not On Hold");
            }else {
                console.log("Pushing Into Hold");
                filteredArray.push(secondAssessmentOnHold[secondOnHold]);
            }

        }
    }
    if(interactionStatus == 'Third_Assessment_OnHold_Internal') {
        //console.log("CV_Send_Pending");
        for (var thirdOnHold in thirdAssessmentOnHold)
        {
            //console.log(activeCVCount++);
            //console.log(activeInteraction);
            if(thirdOnHold in thirdAssessmentNotOnHold){
                //console.log("Rejected");
            }else {
                //console.log("Active");
                filteredArray.push(thirdAssessmentOnHold[thirdOnHold]);
            }

        }
    }
    var candidateData = JSON.stringify(filteredArray);
    //console.log("After Stringify");
    //console.log(JSON.stringify(candidateData));
    var candidateJSObject = JSON.parse(candidateData);
    //console.log(candidateJSObject);
    //console.log("candidateJSObject" + candidateJSObject.length);
   return candidateJSObject;
}
