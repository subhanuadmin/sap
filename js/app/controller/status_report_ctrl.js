/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */


function StatusReportCtrl($scope,$window,$rootScope,notify) {
    console.log('StatusReportCtrl');

    $scope.loadDailyStatusReport = function () {
        console.log("Load Daily Status");
        $window.open("https://drive.google.com/drive/u/0/folders/0BxYvrrMah1kAUENBUG5PYzIxUTg", '_target');

    };

    $scope.loadWeeklyStatusReport = function () {
        console.log("Load Daily Status");
        $window.open("https://drive.google.com/open?id=0B8HfkhmgFK9xNUQwamJhR29LODg", '_target');

    };

    $scope.loadTaskStatusReport = function () {
        console.log("Load Task Status");
        $window.open("https://docs.google.com/spreadsheets/d/1ZXMJnxI-L9PvDVdsYlNn69_d82X_v5VWRLfzHO9FqkY/edit#gid=0&vpid=A34", '_target');

    };

    $scope.loadFeedbackStatusReport = function () {
        console.log("Load Feedback Status");
        $window.open("https://docs.google.com/spreadsheets/d/1r4AA-KJVY98dUxjtU8T_uXhm3jWPd6KFYbfvWmBK7m0/edit#gid=0&vpid=A1", '_target');

    };
}