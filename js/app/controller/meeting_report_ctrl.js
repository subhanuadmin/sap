/**
 * Created by snehita on 12/17/2015.
 */
function MeetingReportCtrl($scope,$rootScope,notify,$filter,$window) {
    console.log("MeetingReportCtrl");
    $scope.activateMeetingTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.meets = [{
        "Timestamp": "2015-02-28T18:30:00.000Z",
        "Username": "Bhanu",
        "Meeting_Date": "2015-02-28T18:30:00.000Z",
        "Meeting_Type": "F2F",
        "Attendees": "Reshma",
        "Meeting_Agenda": "BBC"
    }, {
        "Timestamp": "2015-02-28T18:30:00.000Z",
        "Username": "Bhanu",
        "Meeting_Date": "2015-02-28T18:30:00.000Z",
        "Meeting_Type": "F2F",
        "Attendees": "Reshma",
        "Meeting_Agenda": "BBC"
    }];

    $scope.$watch('activateMeetingTable', function () {
        // alert('hey, myVar has changed!');
    });

    $scope.getMeetingReportDetails = function () {
        $scope.meetingReportLoadingStatus = "Loading";
        console.log("loadMeetingReport");
        var startDateWithSlash = $filter('date')($scope.a, "yyyy/MM/dd");
        var endDateWithSlash = $filter('date')($scope.b, "yyyy/MM/dd");
        var startDateWithHyphen = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDateWithHyphen = $filter('date')($scope.b, "yyyy-MM-dd");

        console.log("Selected - Start Date -> " + startDateWithSlash);
        console.log("Selected - End Date -> " + endDateWithSlash);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);



        $scope.meets = [{
            "Timestamp": "2015-02-28T18:30:00.000Z",
            "Username": "Bhanu",
            "Meeting_Date": "2015-02-28T18:30:00.000Z",
            "Meeting_Type": "F2F",
            "Attendees": "Reshma",
            "Meeting_Agenda": "BBC"
        }, {
            "Timestamp": "2015-02-28T18:30:00.000Z",
            "Username": "Bhanu",
            "Meeting_Date": "2015-02-28T18:30:00.000Z",
            "Meeting_Type": "F2F",
            "Attendees": "Reshma",
            "Meeting_Agenda": "BBC"
        }];


        var queryUrl = getMeetingReportURL($scope.talentSpecialist, startDateWithHyphen, endDateWithHyphen,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);


        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.meets = formattedData.ResultSet;
            console.log("$scope.meets -> " + $scope.meets);
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.meetingReportLoadingStatus = "";
                $scope.activateMeetingTable = true;
            });
        });

    }
}