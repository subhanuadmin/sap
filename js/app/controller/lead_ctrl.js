/**
 * Created by snehita on 12/16/2015.
 */
function LeadCtrl($scope,$rootScope,$filter,$modal,$window) {
    console.log('LeadReportCtrl');

    $scope.getOperationsLeadResponsibility = function () {
        console.log("Load Operations Lead Responsibility Status");
        $window.open("https://drive.google.com/open?id=13dzlFghOkhI7AuDCLY2e2dp1sclVrtsjpEakYOttk9o", '_target');

    };
    $scope.getTeamLeadResponsibility = function () {
        console.log("Load Team Lead Status");
        $window.open("https://drive.google.com/open?id=17ajg65tbzdAZouGSiWMf0APpL1JwNUdGBJv-4XBUZNE", '_target');

    };

    $scope.getProductivity = function () {
        console.log("Load Productivity Status");
        $window.open("https://drive.google.com/open?id=1X_SdiP7qTjcmqb6eQ7R0p08EugGzx3-U4GjkmCpcWiA", '_target');

    };

    $scope.getReports = function () {
        console.log("Load Reports");
        $window.open("https://drive.google.com/open?id=1xawjVR3q3ij7R2HnE0W0KUJht3q0jKxgRD1JVyHZjkc", '_target');

    };
    $scope.getDailyTime = function () {
        console.log("Load Login/Logout Status");
        $window.open("https://drive.google.com/open?id=11WsmX2rxiJPXp-At3Va1IrfWt0YrMiJCxsaPwIpJlac", '_target');

    };
    $scope.getIndividualTime = function () {
        console.log("Load Individual Login/Logout Status");
        $window.open("https://drive.google.com/open?id=1X_SdiP7qTjcmqb6eQ7R0p08EugGzx3-U4GjkmCpcWiA", '_target');

    };



}