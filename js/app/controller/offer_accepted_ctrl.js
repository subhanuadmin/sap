/**
 * Created by Malini on 12-Mar-15.
 */


 function OfferAcceptedCtrl($scope,$window,$filter,$rootScope) {
    console.log('Offer Accepted' );
    $scope.activateScheduleTable = false;
    console.log('active schedule table');
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.$watch('activateScheduleTable', function() {
        // alert('hey, myVar has changed!');
    });

    //Loading Dropdown on view
    $scope.initOfferAccepted = function () {
        console.log('Init Offer Accepted');
        var masterResults = {
            "Open": [{
                "Client": "Big Basket(BBC)",
                "Role": "Python Developer",
                "RequirementID": "BBC-JNR-PYT",
                "No_of_positions": 1,
                "Req_Open_Date": "",
                "Status": "Offered",
                "Last_Date_Checked_with_Client": "",
                "Posn_CV_Ratio": "",
                "CV's_Required": 0,
                "Min_Salary": 900000,
                "Max_Salary": 1100000,
                "Avg_Salary": 1000000,
                "Subhanu_Svc_Chg": 0.0833,
                "Invoice_%": 0.0833,
                "Service_Tax": 0.1236,
                "Requirement_Sequence": "",
                "Position_Sequence": "",
                "Priority": ""
            }, {
                "Client": "",
                "Role": "QA",
                "RequirementID": "BBC-JNR-QA",
                "No_of_positions": 1,
                "Req_Open_Date": "2015-01-28T00:00:00.000Z",
                "Status": "Open",
                "Last_Date_Checked_with_Client": "",
                "Posn_CV_Ratio": 3,
                "CV's_Required": 3,
                "Min_Salary": 600000,
                "Max_Salary": 800000,
                "Avg_Salary": 700000,
                "Subhanu_Svc_Chg": 0.0833,
                "Invoice_%": 0.0833,
                "Service_Tax": 0.1236,
                "Requirement_Sequence": "",
                "Position_Sequence": "",
                "Priority": ""
            }]
        };


        $scope.masterList = masterResults.Open;
        console.log($rootScope.activeRequirements.length);
        if ($rootScope.activeRequirements != null && $rootScope.activeRequirements.length == 0 ) {
            console.log("Getting Requirements ");
            var queryUrl = getRequirementsMasterDocsQueryURL($rootScope.root_stored_token);
            console.log("queryUrl -> " + queryUrl);
            var query = new google.visualization.Query(queryUrl);

            // Send the query with a callback function.
            query.send(function handleQueryResponse(response) {
                if (response.isError()) {
                    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }
                data = response.getDataTable();
                var jsonData = data.toJSON();
                var parsedJSONData = JSON.parse(jsonData);
                var formattedData = formatResult(parsedJSONData);
                $rootScope.activeRequirements = formattedData.ResultSet;
                console.log("Loaded to Cache");
                console.log($rootScope.activeRequirements);
                $scope.masterList = $rootScope.activeRequirements;
            });
        }else {
            console.log("Picking from cache");
            $scope.masterList = $rootScope.activeRequirements;
            //console.log(JSON.stringify($scope.masterList));
        }


    };

    //Processing on click of button
    $scope.getOfferAccepted = function () {
        console.log('getOfferAccepted');
        $scope.scheduleTableLoadingStatus = "Loading";
        console.log("Get Offer Accepted");
        console.log("Selected requirementID -> " + $scope.requirement.Requirement_ID);
        var startDate = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDate = $filter('date')($scope.b, "yyyy-MM-dd");
        console.log("Selected - Start Date -> " + startDate );
        console.log("Selected - End Date -> " + endDate);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);
        // var queryUrl = getJoiningCandidateReportQueryURL($scope.requirement.Requirement_ID,$scope.talentSpecialist,startDate,endDate);
        var queryUrl = getOfferAcceptedQueryURL($scope.requirement.Requirement_ID,$scope.talentSpecialist,startDate,endDate,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            // var jsonData = data.toJSON();
            var table = new google.visualization.Table(document.getElementById("querytable"));
            //console.log("Table Initialised");
            var monthYearFormatter = new google.visualization.DateFormat({ pattern: "MMMM d, yyyy"});
            monthYearFormatter.format(data, 4);
            table.draw(data, {
                allowHtml:true,
                showRowNumber: false,
                page : 'enable',
                pageSize:20,
                sortColumn: 5,
                sortAscending:true
            });
            $scope.$apply(function () {
                $scope.scheduleTableLoadingStatus = "";
                $scope.activateScheduleTable = true;

            });
        });

    };
}
