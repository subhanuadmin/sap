/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function JoiningReportCtrl($scope,$window,MasterListService,$filter) {
    console.log('InterviewScheduleCtrl');
    $scope.activateScheduleTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.$watch('activateScheduleTable', function() {
        // alert('hey, myVar has changed!');
    });

    //Loading Dropdown on view
    $scope.initJoiningReport = function () {
        console.log("Init Joining Report");
        var masterResults = {"Open":[{"Client":"Big Basket(BBC)","Role":"Python Developer","RequirementID":"BBC-JNR-PYT","No_of_positions":1,"Req_Open_Date":"","Status":"Offered","Last_Date_Checked_with_Client":"","Posn_CV_Ratio":"","CV's_Required":0,"Min_Salary":900000,"Max_Salary":1100000,"Avg_Salary":1000000,"Subhanu_Svc_Chg":0.0833,"Invoice_%":0.0833,"Service_Tax":0.1236,"Requirement_Sequence":"","Position_Sequence":"","Priority":""},{"Client":"","Role":"QA","RequirementID":"BBC-JNR-QA","No_of_positions":1,"Req_Open_Date":"2015-01-28T00:00:00.000Z","Status":"Open","Last_Date_Checked_with_Client":"","Posn_CV_Ratio":3,"CV's_Required":3,"Min_Salary":600000,"Max_Salary":800000,"Avg_Salary":700000,"Subhanu_Svc_Chg":0.0833,"Invoice_%":0.0833,"Service_Tax":0.1236,"Requirement_Sequence":"","Position_Sequence":"","Priority":""}]};
        $scope.masterList = masterResults.Open;
        var masterListPromise = MasterListService.getMasterList();
        masterListPromise.then(function (response) {  // this is only run after $http completes
            console.log("Inside Promise - Status is " + response.status);
            if(response.status == 200){
                masterResults = response.data;
                $scope.masterList = masterResults.Open;
                console.log(JSON.stringify($scope.masterList));
            }else{
                $window.alert("Fetching Requirements failed");
            }

        });
    };

    //Processing on click of button
    $scope.getJoiningDetails = function () {
        console.log("Get Interview Schedule");
        console.log("Selected requirementID -> " + $scope.requirement.Requirement_ID);
        var startDate = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDate = $filter('date')($scope.b, "yyyy-MM-dd");
        console.log("Selected - Start Date -> " + startDate );
        console.log("Selected - End Date -> " + endDate);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);
        var queryUrl = getInterviewScheduleQueryURL($scope.requirement.Requirement_ID,$scope.talentSpecialist,startDate,endDate,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.schedules = formattedData.ResultSet;
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.activateScheduleTable = true;

            });
        });

    };
}