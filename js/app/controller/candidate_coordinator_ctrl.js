/**
 * Created by snehita on 12/3/2015.
 */
function CandidateCoordinatorCtrl($scope, $window) {
    console.log('CandidateCoordinatorReportCtrl');

    $scope.getResponsibility = function () {
        console.log("Load Responsibility Status");
        $window.open("https://drive.google.com/open?id=1Nb8VPUEZCDFTaeQUkhPslUt-aiPNSYiEuX8A_D8rysE", '_target');

    };
    $scope.getFollowUpReport = function () {
        console.log("Load Candidate Coordinator Status");
        $window.open("https://docs.google.com/spreadsheets/d/1AZZbw-Gjy7imiou411Ig3aVDPEAVBGxRK8y6gOevX7w/edit#gid=499398393&vpid=A1", '_target');

    };

    $scope.getInterviewSummary = function () {
        console.log("Load Interview Summary Status");
        $window.open("https://docs.google.com/spreadsheets/d/12shb4PUBIYHjiejbRfSbnWgl0bdar_R2TZOq92-Sv2Q/edit#gid=1342568049&vpid=A1", '_target');

    };
    $scope.getStatusReport = function () {
        console.log("Load Status Report");
        $window.open("https://docs.google.com/spreadsheets/d/1g77mX95u4jTN3aGEAF1xaxkoZhisIvkcISc0WhChH0c/edit#gid=1667388303", '_target');

    };
    $scope.getTemplates = function () {
        console.log("Load Templates");
        $window.open("https://drive.google.com/drive/folders/0BxYvrrMah1kAVXRQWHdDMVJobzg", '_target');

    };

}