/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */

function RequirementMasterCtrl($scope,$rootScope) {
    console.log('RequirementMasterCtrl');
    $scope.requirementMasterLoadingStatus = "Loading";
    $scope.requirementMasterTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.$watch('requirementMasterTable', function() {
        // alert('hey, myVar has changed!');
    });

    $scope.loadRequirements = function () {
        console.log("get All Requirements");
        var requirementResults = {"OpenRequirements":[{"Client":"BBC","Role":"Python Developer","Requirement_ID":"BBC-JNR-PYT","Positions":"2","Open_Date":"02-Feb-2015","Status":"Open","CV_Required":"1"}]};
        $scope.openRequirements = requirementResults.OpenRequirements;
        if ($rootScope.activeRequirements != null && $rootScope.activeRequirements.length == 0 ) {
            var queryUrl = getRequirementsMasterDocsQueryURL($rootScope.root_stored_token);
            console.log("queryUrl -> " + queryUrl);
            var query = new google.visualization.Query(queryUrl);

            // Send the query with a callback function.
            query.send(function handleQueryResponse(response) {
                if (response.isError()) {
                    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }
                data = response.getDataTable();
                var jsonData = data.toJSON();
                var parsedJSONData = JSON.parse(jsonData);
                var formattedData = formatResult(parsedJSONData);
                $rootScope.activeRequirements = formattedData.ResultSet;
                console.log("Loaded to Cache");
                console.log($rootScope.activeRequirements);
                $scope.$apply(function () {
                    console.log("Apply Called");
                    $scope.requirementMasterLoadingStatus = "";
                    $scope.requirementMasterTable = true;

                });
            });
        }else {
            console.log("Picking from cache");
            $scope.openRequirements = $rootScope.activeRequirements;
            $scope.requirementMasterTable = true;
        }
    };

    $scope.edit = function(openRequirement) {
        console.log("Edit " + openRequirement.Requirement_ID);
        $rootScope.openRequirement=openRequirement;
        console.log("Rootscope " + $rootScope.openRequirement.Requirement_ID);
    }

    $scope.loadRequirementDetails = function () {
        console.log("Rootscope " + $rootScope.openRequirement.Requirement_ID);
        $scope.openRequirement = $rootScope.openRequirement;
        console.log("From Local scope" + JSON.stringify($scope.openRequirement));
    };
}