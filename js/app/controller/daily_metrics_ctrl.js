/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */
function DailyMetricsMasterCtrl($rootScope,$scope,$window) {
    console.log('DailyMetricsMasterCtrl');
    $scope.dailyMetricsMasterLoadingStatus = "Loading";
    $scope.activateDailyMetricsMasterTable = false;
    var stored_token = $window.localStorage.getItem("token");
    console.log("stored_token" + stored_token);
    $rootScope.root_stored_token=stored_token ;
    console.log("happened");

    $scope.loadDailyMetrics = function () {
        console.log("get All Metrics");
        var dailyMetricsResults = {"DailyMetrics":[{"Work_Date":"2014-12-30T18:30:00.000Z","Requirement_ID":"FSNL-JNR-JAVA","Job_Boards_Posted":0,"Responses_Received":15,"Responses_Processed":"","Targetted_Mails":120,"Direct_Mails":30,"Cold_Calls":15,"First_Assessment":2,"Second_Assesment":"","Other_Calls":"","Documents_Created":0,"Workaids_Created":0,"Resumes_Assessed":0,"Resumes_Shortlisted":0,"Username":"sushma@subhanu.com"}]};
        $scope.dailyMetrics = dailyMetricsResults.DailyMetrics;

        var queryUrl = getDailyMetricsMasterQueryURL(stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.dailyMetrics = formattedData.ResultSet;
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.dailyMetricsMasterLoadingStatus = "";
                $scope.activateDailyMetricsMasterTable = true;

            });
        });
    };


    $scope.edit = function(dailyMetric) {
        $rootScope.dailyMetricsDetails=dailyMetric;
        console.log("Rootscope " + $rootScope.dailyMetricsDetails);
    }

    $scope.loadDailyMetricsDetails = function () {
        console.log("load Dail yMetrics Details");
        console.log("Rootscope " + $rootScope.dailyMetricsDetails);
        $scope.dailyMetric = $rootScope.dailyMetricsDetails;
        console.log("From Local scope" + $scope.dailyMetric);
    };
}