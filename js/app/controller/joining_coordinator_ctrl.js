/**
 * Created by snehita on 12/4/2015.
 */

function JoiningCoordinatorCtrl($scope,$rootScope,$filter,$modal,$window) {
    console.log('JoiningCoordinatorReportCtrl');

    $scope.getResponsibility = function () {
        console.log("Load Responsibility Status");
        $window.open("https://drive.google.com/open?id=1ROTxtlfhZiHpc-hCPy8STcm8lGUOYdrr418MHaCg_hQ", '_target');
    };
    $scope.getJoiningFollowup = function () {
        console.log("Load Joining Follow Up Status");
        $window.open("https://docs.google.com/spreadsheets/d/1pdqW1UpDGUIZRbCsMjXr7IGQG0FS_Heaeb2KggtIXcI/edit#gid=1189027343&vpid=A1", '_target');

    };

    $scope.getWeekwiseJoiningReport = function () {
        console.log("Weekwise Joining");
        $window.open("https://docs.google.com/spreadsheets/d/188PDeSJIbOSRpqtdLBWT-NsO9IOARz-lT_AmUN6HDpI/edit#gid=1966589386&vpid=A1", '_target');

    };
    $scope.getMonthwiseOfferAceeptedReport = function () {
        console.log("Monthwise Offer Accepted");
        $window.open("https://docs.google.com/spreadsheets/d/1RiGgJQASR5ZaYncaxwYD1c7fREmaDGGatn27na1Np_M/edit#gid=548345631&vpid=A1", '_target');

    };

    $scope.getMonthwiseJoiningReport = function () {
        console.log("Monthwise Joining");
        $window.open("https://docs.google.com/spreadsheets/d/1a23QghW-k8eiGSrugL_Naeb_MApoIhVb1VB9sl514Rg/edit#gid=548345631&vpid=A1", '_target');

    };

    $scope.getJoiningStatusReport = function () {
        console.log("Load Joining Co ordinator Status Report");
        $window.open("https://docs.google.com/spreadsheets/d/1h089Qe93qfqpWZ7L2979GRYGFj6q7LaY9cR6p-yNn4Y/edit", '_target');

    };

    $scope.getTemplates = function () {
        console.log("Load Joining Co ordinator Templates");
        $window.open("https://drive.google.com/drive/folders/0BxYvrrMah1kAa3ZTck1Rdnh0ZmM", '_target');

    };
}