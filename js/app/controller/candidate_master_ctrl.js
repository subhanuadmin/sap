/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */

function CandidateMasterCtrl($scope, $location, $rootScope, $window,CandidateMasterQueryService) {
    console.log("CandidateMasterCtrl");
    $scope.candidateMasterLoadingStatus = "Loading";
    $scope.activateCandidateMasterTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);


    $scope.$watch('activateCandidateMasterTable', function() {
        // alert('hey, myVar has changed!');
    });

    $scope.loadCandidateMaster = function () {
        console.log("loadCandidateMaster");
         var masterResults =  {"ResultSet":[{"Requirement_ID":"ILA-SNR-COD","Candidate_ID":"CAND-545","Name":"MEENA BISWA","Contact_Number":"MEENA BISWA","Email_ID":"meeniminyong@yahoo.com","Candidate_Rating":"Good"},{"Requirement_ID":"INRC-SNR-CSE-BY","Candidate_ID":"CAND-544","Name":" Jagadish Patra","Contact_Number":7204856175,"Email_ID":"jagadishpatra6@gmail.com","Candidate_Rating":"Good"}]};
        $scope.candidates = masterResults.ResultSet;
        //console.log(JSON.stringify($scope.candidates));

        var queryUrl = getCandidatesMasterQueryURL("All","All",$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.candidates = formattedData.ResultSet;
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.candidateMasterLoadingStatus = "";
                $scope.activateCandidateMasterTable = true;

            });
        });
    };

    $scope.edit = function(candidate) {
        console.log("Edit " + candidate.Candidate_ID);
        $rootScope.candidateDetails=candidate;
        console.log("Rootscope " + $rootScope.candidateDetails.Candidate_ID);
    }

    $scope.isCVPresent = function(candidate) {
        var cvPresent = false;
        var cvLink = candidate.Subhanu_Link;
        console.log("Subhanu CV " + cvLink);
        if( cvLink != null){
            cvPresent = true;
        }
        return cvPresent;
    }

    $scope.loadDetails = function () {
        console.log("load Details");
        console.log("Rootscope " + $rootScope.candidateDetails.Candidate_ID);
        $scope.candidateDetails = $rootScope.candidateDetails;
        console.log("From Local scope" + JSON.stringify($scope.candidateDetails));
    };
};