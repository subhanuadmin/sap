/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */

/**
 * MainCtrl - controller
 */
function MainCtrl($rootScope,$scope,$window) {

    console.log("Main Called");
    this.userName = 'Subhanu user';
    this.designation = 'Subhanu Designation';
    this.helloText = 'Welcome to Subhanu';
    this.descriptionText = 'Subhanu Application Pro';
    $rootScope.activeRequirements = null;
    $scope.activateUserName = false;
    $rootScope.loggedInUser = null;
    $scope.feedbacks = {};
	var stored_token = $window.localStorage.getItem("token");
    console.log("stored_token" + stored_token);
    $rootScope.root_stored_token=stored_token ;
    console.log("happened");




    $scope.$watch('activateUserName', function() {
        // alert('hey, myVar has changed!');
    });


    $scope.feedbackColDefs = [{field: 'Feedback_Type', displayName: 'Feedback Type', width: "auto",resizable: true},
        { field: 'Feedback_Category', displayName: 'Category', width: "auto",resizable: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><a ui-sref="index.feedback_details">{{row.getProperty(col.field)}}</a></div>' },
        { field: 'Repeated', displayName: 'Repeated', width: "auto" ,resizable: true},
        { field: 'Feedback', displayName: 'Feedback', width: "auto" ,resizable: true}];

    $scope.ngOptions2 = {
        data: 'feedbacks',
        columnDefs: 'feedbackColDefs',
        showGroupPanel: true,
        jqueryUIDraggable: true

    };

    console.log("Getting Requirements ");
    //var queryUrl = getRequirementsMasterQueryURL(stored_token);
    var queryUrl = getRequirementsMasterDocsQueryURL(stored_token);
    console.log("queryUrl -> " + queryUrl);
    var query = new google.visualization.Query(queryUrl);

    // Send the query with a callback function.
    query.send(function handleQueryResponse(response) {
        if (response.isError()) {
            alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
            return;
        }
        data = response.getDataTable();
        var jsonData = data.toJSON();
        var parsedJSONData = JSON.parse(jsonData);
        var formattedData = formatResult(parsedJSONData);
        $rootScope.activeRequirements = formattedData.ResultSet;
        console.log("Loaded to Cache");
        console.log($rootScope.activeRequirements);
    });

    $scope.setUser = function(){
        console.log("setUser " + this.user);
        if(this.user == 'bhanu'){
            console.log("bhanu");
            $scope.userName = 'Bhanumathy Yellapan';
            $scope.displayName = 'Bhanumathy Yellapan';
            $scope.designation = 'CEO and Founder';
            $scope.imageName = 'bhanu.jpg';
            $scope.gid = "513112741";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'bhanu@subhanu.com';
            $rootScope.isFTE = false;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1T28ATm67r2ur2ahQ7yPPSIcRPeZFOQ05K18NRd9T_eM&authuser=0";
        }
        if(this.user == 'bhoopalan'){
            console.log("bhoopalan");
            $scope.userName = 'Bhoopalan Y Padua';
            $scope.displayName = 'Bhoopalan';
            $scope.designation = 'Co-Founder and Mentor';
            $scope.imageName = 'bhoopalan.jpg';
            $scope.gid = "1053336599";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'bhoopalan@subhanu.com';
            $rootScope.isFTE = false;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1tyUnRXHIY18KVdCpueFCRqwH--cxVD6-fNDIXobQ6GQ&authuser=0";
        }
       /* if(this.user == 'rashmi'){
            console.log("rashmi");
            $scope.userName = 'Rashmi Gundalli';
            $scope.displayName = 'Rashmi Gundalli';
            $scope.designation = 'Senior Talent Specialist';
            $scope.imageName = 'rashmi.jpg';
            $scope.gid = "147035376";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'rashmi@subhanu.com';
            $rootScope.isFTE = false;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1wALSKcm8XGkx4bJxOuTn4Bm1hrJ6m6DfxnjJc_u2E0M&authuser=0";
        }
       */

        if(this.user == 'vidyashree.naik'){
            console.log("vidyashree.naik");
            $scope.userName = 'Vidyashree Naik';
            $scope.displayName = 'vidyashree Naik';
            $scope.designation = 'Junior Talent Specialist';
            $scope.imageName = 'vidyashreenaik.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'vidyashree.naik@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=18dsGWty4J1p7ZwfF5p6MwdmlDxyMlbXL0arI3aiX3Qw&authuser=0";
        }
        if(this.user == 'reshma.hebbar'){
            console.log("reshma.hebbar");
            $scope.userName = 'Reshma Hebbar';
            $scope.displayName = 'Reshma Hebbar';
            $scope.designation = 'Junior Talent Specialist';
            $scope.imageName = 'reshmahebbar.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'reshma.hebbar@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1NmSmSMEkVmiPYHgWlqV1RTRUrclikAAv9P3tNM7GaCc&authuser=0";
        }

        if(this.user == 'shubhasree.n'){
            console.log("shubhasree.n");
            $scope.userName = 'Shubhasree Narasimhan';
            $scope.displayName = 'Shubhasree Narasimhan';
            $scope.designation = 'Senior Talent Specialist';
            $scope.imageName = 'shubhasreen.jpg';
            $scope.gid = "828959801";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'shubhasree.n@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1NmSmSMEkVmiPYHgWlqV1RTRUrclikAAv9P3tNM7GaCc&authuser=0";
        }



        if(this.user == 'chaitra.prashanth'){
            console.log("chaitra.prashanth");
            $scope.userName = 'Chaithra Prashanth';
            $scope.displayName = 'Chaitra Prashanth';
            $scope.designation = 'Senior Talent Specialist';
            $scope.imageName = 'ChaitraPrashanth.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'chaithra.prashanth@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1NmSmSMEkVmiPYHgWlqV1RTRUrclikAAv9P3tNM7GaCc&authuser=0";
        }
       /* if(this.user == 'Rashmi.Kilarimath'){
            console.log("Rashmi Kilarimath");
            $scope.userName = 'Rashmi Kilarimath';
            $scope.displayName = 'Rashmi Kilarimath';
            $scope.designation = 'Junior Talent Specialist';
            $scope.imageName = 'rashmiK.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'rashmi.k@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1NmSmSMEkVmiPYHgWlqV1RTRUrclikAAv9P3tNM7GaCc&authuser=0";
        }*/
        if(this.user == 'harshitha.j'){
            console.log("Harshitha Jagadesh");
            $scope.userName = 'Harshitha Jagadesh';
            $scope.displayName = 'Harshitha Jagadesh';
            $scope.designation = 'Junior Talent Specialist';
            $scope.imageName = 'harshitha.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'harshitha.j@subhanu.com';
            $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://drive.google.com/open?id=1NmSmSMEkVmiPYHgWlqV1RTRUrclikAAv9P3tNM7GaCc&authuser=0";
        }

        $scope.welcomeText = 'Jai Gurudev ' + $scope.displayName;
        $scope.welcomeDescription = 'Have a good productive day!!';
        console.log("Getting Feedback ");

  /*      var feedbackResults = [{"Timestamp":"Date(2015, 2, 3)","Feedback Date":"Date(2015, 2, 2)","Username":"itadmin@subhanu.com","Feedback_On":"chaitra","Feedback":"Filled up Old Subhanu Workday\nNot attentive during meeting.\nMeeting Actions not followed","Feedback_Action":"Be Attentive and make note of changes\nReview Notes after Meeting","Feedback_Rating":2,"Feedback_Category":"Process-Complaince","Feedback_Type":"Get-Better"}];
        $scope.feedbacks = this.feedbackResults;*/

        var queryUrl = getFeedbackMasterQueryURL('All',this.user,'','',stored_token );
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            console.log(formattedData);
            $scope.feedbacks = formattedData.ResultSet;
            console.log($scope.feedbacks);

            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.activateUserName = true;
            });
        });

        $scope.edit = function(allFeedbacks, rowIndex) {
            console.log("Edit " + rowIndex);
            console.log("Edit " + allFeedbacks);
            $rootScope.feedbackDetails=$scope.feedbacks[rowIndex];
           // console.log("Rootscope " + $rootScope.feedbackDetails.feedback);
        }
    }
};