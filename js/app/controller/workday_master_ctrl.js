/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */
function WorkDayMasterCtrl($scope,$rootScope,notify,$filter,$window) {
    console.log('WorkDayMasterCtrl');
    $scope.activateworkdayMasterTable = false;




    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);



    $scope.workdays = [{"Workdate":"2015-02-28T18:30:00.000Z","Login":"09:00","Logout":"06:00","BreakTime":"10","SystemComments":"Weekly Off","user":"holiday@subhanu.com"}];

    $scope.$watch('activateworkdayMasterTable', function() {
        // alert('hey, myVar has changed!');
    });

    $scope.getWorkdaySummary = function () {
        $scope.wordayMasterMasterLoadingStatus = "Loading";
        console.log("Get Workday Summary");
        var startDateWithSlash = $filter('date')($scope.a, "yyyy/MM/dd");
        var endDateWithSlash = $filter('date')($scope.b, "yyyy/MM/dd");
        var startDateWithHyphen = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDateWithHyphen = $filter('date')($scope.b, "yyyy-MM-dd");

        console.log("Selected - Start Date -> " + startDateWithSlash );
        console.log("Selected - End Date -> " + endDateWithSlash);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);
        $scope.workdays = [{"Workdate":"2015-02-28T18:30:00.000Z","Login":"10:00","Logout":"5:00","BreakTime":"10","SystemComments":"Weekly Off","user":"holiday@subhanu.com"}];

        //$rootScope.userId = 'bhanu@subhanu.com';
        $scope.isFTE = $rootScope.isFTE;
        if ($rootScope.userId == null){
            notify({
                message:'Please set user',
                classes: 'alert-danger'

            })
        } else {
           var queryUrl = getWorkdayMasterQueryURL($rootScope.userId,startDateWithHyphen,endDateWithHyphen,$rootScope.root_stored_token);
           // var queryUrl = getWorkdayMasterQueryURL($rootScope.userId,'2015-03-01', '2015-03-31');
            console.log("queryUrl -> " + queryUrl);
            var query = new google.visualization.Query(queryUrl);

            // Send the query with a callback function.
            query.send(function handleQueryResponse(response) {
                if (response.isError()) {
                    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }
                data = response.getDataTable();
                var jsonData = data.toJSON();
                var parsedJSONData = JSON.parse(jsonData);
                var formattedData = formatResult(parsedJSONData);
                var results = formattedData.ResultSet;
                if (typeof results == "undefined") {
                    console.log("Could not Fetch Results");
                    $scope.wordayMasterMasterLoadingStatus = "No Data Found ";
                } else {
                    console.log("Results fetched successfully from Workday Master");
                   //console.log(results);
                    $scope.workdaySummary = getWorkdaySummary(results, $rootScope.userId,startDateWithSlash, endDateWithSlash);
                    //$scope.workdaySummary = getWorkdaySummary(results, 'bhanu@subhanu.com','2015/03/01', '2015/03/31');
                    $scope.workdays = $scope.workdaySummary.WorkDays;
                   //console.log(JSON.stringify($scope.workdays));
                }
                $scope.$apply(function () {
                   //console.log("Apply Called");
                    $scope.wordayMasterMasterLoadingStatus = "";
                    $scope.activateworkdayMasterTable = true;

                });
            });
        }

    };

    $scope.loadLeavePlan = function () {
        console.log("Load leave plan");
       // $window.open("https://drive.google.com/open?id=1lKX-gtPm7eVT44Ej4y1VvWVuYUZez5-rtQj6scIqR0Q", '_target');
        $window.open("https://docs.google.com/spreadsheets/d/1rZwm9Nu6Bph28LlRKo4giG7S-HRq9uD1wbCHIMz4A_M", '_blank');

    };

    $scope.loadPaybooks = function () {
        console.log("Load Paybooks");
        $window.open("https://apps.paybooks.in/mylogin.aspx", '_target');

    };
    $scope.loadPolicy = function () {
        console.log("Load Subhanu policy");
        $window.open("https://docs.google.com/presentation/d/19ZWJyoib_19XO0L5XEYYEezFKHWPkwKt4QxhC6BPVAo/edit#slide=id.gd3eed0521_1_5", '_target');

    };
    $scope.loadResponsibilities = function () {
        console.log("Load Subhanu Responsibilities");
        $window.open("https://drive.google.com/drive/u/0/folders/0BxYvrrMah1kAYzdUZl9RT2h3bkk", '_target');

    };
}