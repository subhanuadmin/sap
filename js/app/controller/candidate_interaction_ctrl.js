/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */
function CandidateInteractionCtrl($rootScope,$scope,$window) {
    console.log('CandidateInteractionCtrl');

    $scope.candidateInteractionLoadingStatus = "Loading";
    $scope.activateCandidateInteractionTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.$watch('activateCandidateInteractionTable', function() {
        // alert('hey, myVar has changed!');
    });

    $scope.loadInteractions = function () {
        console.log("get All Interactions");
        var interactionResults = {"ResultSet":[{"Requirement_ID":"SP-JNR-JAVA","Candidate_ID":"CAND-450","Name":"Bhaskar  Gardasu\t","Contact_Number":"+91-7709755798","Interaction_Date":"+91-7709755798","Candidate_Status":"Joined","Interaction_Comments":"I have been interacting with Bhaskar the last 3 days - Mon, Tuesday & Wednesday.\nto ensure he is joining,\nto collect references\nto confirm his joining. \nGood candidate - kept his word and joined."},{"Requirement_ID":"SP-SNR-LEAD","Candidate_ID":"CAND-541","Name":"Kaushal Mandal","Contact_Number":"9535032541","Interaction_Date":"9535032541","Candidate_Status":"CV Sent to Client","Interaction_Comments":"CV sent to client"},{"Requirement_ID":"INRC-SNR-CSE-BY","Candidate_ID":"CAND-544","Name":" Jagadish Patra","Contact_Number":"7204856175","Interaction_Date":"7204856175","Candidate_Status":"17_Client Interview Scheduled","Interaction_Comments":"CAND-544"},{"Requirement_ID":"SP-SNR-LEAD","Candidate_ID":"CAND-556","Name":"Swetabh Raj","Contact_Number":"8860099071","Interaction_Date":"8860099071","Candidate_Status":"05_Resume Shortlisted (Internal)","Interaction_Comments":"Yet to send CV to client "}]};
        console.log("loadInteractions");
        $scope.candidateInteractions = interactionResults.ResultSet;
        //$scope.activateCandidateInteractionTable = true;
        //console.log(JSON.stringify($scope.candidates));

        var queryUrl = getCandidateInteractionQueryURL("All","All",$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.candidateInteractions = formattedData.ResultSet;
            $scope.$apply(function () {
                //console.log("Apply Called");
                $scope.candidateInteractionLoadingStatus = "";
                $scope.activateCandidateInteractionTable = true;

            });
        });
    };
    $scope.edit = function(candidateInteraction) {
        console.log("Edit " + candidateInteraction.Candidate_ID);
        $rootScope.candidateInteraction=candidateInteraction;
        console.log("Rootscope " + $rootScope.candidateInteraction.Candidate_ID);
    }
    $scope.loadInteractionDetails = function () {
        console.log("load Details");
        console.log("Rootscope " + $rootScope.candidateInteraction.Candidate_ID);
        $scope.candidateInteraction = $rootScope.candidateInteraction;
        console.log("From Local scope" + JSON.stringify($scope.candidateInteraction));
    };

}