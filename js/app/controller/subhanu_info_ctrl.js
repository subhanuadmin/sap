/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */
function SubhanuInfoCtrl($scope,$rootScope,notify,$filter,$window) {
    console.log('SubhanuInfoCtrl');

    $scope.loadAboutSubhanu= function () {
        console.log("Load about Subhanu");
        $window.open("https://docs.google.com/presentation/d/1mFs18092joT4QN_aI_UeScABkvSviSIrw5BcsZzxdnQ/edit", '_target');
    };

    $scope.loadTrainingDocs= function () {
        console.log("Load Training Docs");
        $window.open("https://drive.google.com/drive/u/0/folders/0BxYvrrMah1kAa1J1bk9SMUdnTWc", '_target');
    };

    $scope.loadTeamContact= function () {
        console.log("Load Team Contact");
        $window.open("https://docs.google.com/spreadsheets/d/122AtDX_IYAycTy2pDgagbiDwWACoAtNtYxf1AZmQ_GM/edit#gid=470481521&vpid=A1", '_target');
    };


}