/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */


function EmailCandidateSummaryCtrl($scope, $filter, $rootScope,$sce,$window) {
    console.log('EmailCandidateSummaryCtrl');
    $scope.subject = "";
    $scope.candidateSummaryTable = false;
    $scope.candidateInteractionSummary = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);


    $scope.firstLine = "";
    $scope.genderAddress = "";
    var has = " has ";
    var experience = " years of experience ";
    $scope.firstLine = "";
    var masterResultStatus = "";
    var interactionResultStatus = "";
    $scope.firstAssessmentComments = "";
    $scope.candidateSummaryTableLoadingStatus = " ";

    $scope.$watch('[candidateSummaryTable, candidateInteractionSummary]', function () {
        // alert("$scope.candidateSummaryTable" +  $scope.candidateSummaryTable);
        //alert("$scope.candidateInteractionSummary" +  $scope.candidateInteractionSummary);
    });

    //Processing on click of button
    $scope.getCandidateSummary = function () {

        $scope.firstLine = "";
         masterResultStatus = "";
         interactionResultStatus = "";
        $scope.firstAssessmentComments = "No comments recorded";
        $scope.secondAssessmentComments = "No comments recorded";

        $scope.candidateSummaryTableLoadingStatus = "Loading";
        $scope.candidateSummaryTable = false;
        $scope.candidateInteractionSummary = false;

        console.log("Get Candidate Summary From Master");
        console.log("Selected candidateID -> " + $scope.candidateID);
        if(typeof $scope.candidateID == "undefined"){
            $scope.candidateSummaryTableLoadingStatus = "Please enter Candidate ID";
            return;
        }
        var queryUrl = getCandidateSummaryQueryURL($scope.candidateID,$rootScope.root_stored_token);
        //var queryUrl = getAllInteractionDetailsQueryURL('',$scope.candidateID,'','','','');
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            var results = formattedData.ResultSet;
            if (typeof results == "undefined") {
                masterResultStatus = "Candidate Not Registered "
            } else {
                $scope.candidateDetailsSummary = results[0];
               // console.log(JSON.stringify($scope.candidateDetailsSummary));
               // console.log("Gender ->" + $scope.candidateDetailsSummary.Gender);
                if ($scope.candidateDetailsSummary.Gender == 'Female') {
                    $scope.genderAddress = "She";
                    $scope.hisher = "Her";
                    $scope.lowerhisher = "her";

                }
                if ($scope.candidateDetailsSummary.Gender == 'Male') {
                    $scope.genderAddress = "He";
                    $scope.hisher = "His";
                    $scope.lowerhisher = "his";
                }

                if ($scope.candidateDetailsSummary.Qualification == null || $scope.candidateDetailsSummary.Qualification == '' ){
                    $scope.candidateDetailsSummary.Qualification = "<B> Missing Qualification </B>";
                }
                if ($scope.candidateDetailsSummary.Experience == null || $scope.candidateDetailsSummary.Experience == ''){
                    $scope.candidateDetailsSummary.Experience = "<B> Missing Experience</B>";
                }
                if ($scope.candidateDetailsSummary.Current_Location == null || $scope.candidateDetailsSummary.Current_Location == '' ||  $scope.candidateDetailsSummary.Current_Location == ' '){
                    $scope.candidateDetailsSummary.Current_Location = "<B> Missing Location</B>";
                }
                if ($scope.candidateDetailsSummary.Current_Salary == null || $scope.candidateDetailsSummary.Current_Salary == '' || $scope.candidateDetailsSummary.Current_Salary == ' '  ){
                    $scope.candidateDetailsSummary.Current_Salary = "<B> Missing Current Salary</B>";
                }
                if ($scope.candidateDetailsSummary.Expected_Salary == null || $scope.candidateDetailsSummary.Expected_Salary == '' || $scope.candidateDetailsSummary.Expected_Salary == ' ' ){
                    $scope.candidateDetailsSummary.Expected_Salary = "<B> Missing Expected_Salary</B>";
                }
                if ($scope.candidateDetailsSummary.Company == null || $scope.candidateDetailsSummary.Company == '' ){
                    $scope.candidateDetailsSummary.Company = "<B> Missing Current Company</B>";
                }
                if ($scope.candidateDetailsSummary.Time_To_Join == null || $scope.candidateDetailsSummary.Time_To_Join == '' || $scope.candidateDetailsSummary.Time_To_Join == ' '){
                    $scope.candidateDetailsSummary.Time_To_Join = "<B> Missing Time To Join</B>";
                }
                $scope.firstLine = "<B>" + $scope.candidateDetailsSummary.Name +"</B>"+ " is a " + "<B>" + $scope.candidateDetailsSummary.Qualification +"</B>";
                $scope.firstLine = $scope.firstLine + " with " + $scope.candidateDetailsSummary.Experience + experience;
                $scope.firstLine = $scope.firstLine + " and is currently located in " + $scope.candidateDetailsSummary.Current_Location + ".";

                $scope.firstLinehtml = $sce.trustAsHtml($scope.firstLine);


                $scope.secondLine =  $scope.genderAddress +  has + $scope.candidateDetailsSummary.Candidate_Rating  + " attitude and " +
                    $scope.candidateDetailsSummary.Communication + " communication.";

                var salaryLine = $scope.hisher + " current salary is " + $scope.candidateDetailsSummary.Current_Salary + " lakhs per annum " + " and  " +
                $scope.lowerhisher + " expected salary is  <B>" + $scope.candidateDetailsSummary.Expected_Salary + "</B> lakhs per annum."

                $scope.salaryLinehtml =  $sce.trustAsHtml(salaryLine);

                if($scope.candidateDetailsSummary.Resignation_Status == "is currently working and not yet resigned") {
                    $scope.employmentStatusLine = $scope.genderAddress  + " is currently employed with " + $scope.candidateDetailsSummary.Company +
                        " and has not yet resigned.";
                }
                if($scope.candidateDetailsSummary.Resignation_Status == "has resigned and is in Notice Period") {
                    $scope.employmentStatusLine = $scope.genderAddress  + " is currently working for" + $scope.candidateDetailsSummary.Company +
                    " and " + $scope.candidateDetailsSummary.Resignation_Status + ".";
                }
                if($scope.candidateDetailsSummary.Resignation_Status == "is currently not working" || $scope.candidateDetailsSummary.Resignation_Status == "is on a Break" ) {
                    $scope.employmentStatusLine = $scope.genderAddress  + " " + $scope.candidateDetailsSummary.Resignation_Status +
                    " and was previously employed with " + $scope.candidateDetailsSummary.Company + ".";
                }

                if($scope.candidateDetailsSummary.Resignation_Status == "is a fresher") {
                    $scope.employmentStatusLine = $scope.genderAddress  + $scope.candidateDetailsSummary.Resignation_Status;
                }

                 var noticePeriodStatusLine = "";
                if ($scope.candidateDetailsSummary.Time_To_Join == "immediately"){
                    noticePeriodStatusLine = $scope.genderAddress + " can join <B>" + $scope.candidateDetailsSummary.Time_To_Join + " </B>.";
                } /*else if($scope.candidateDetailsSummary.Time_To_Join.contains("negotiable")) {
                    $scope.candidateDetailsSummary.Time_To_Join = $scope.candidateDetailsSummary.Time_To_Join.replace(",","(") + ") days."
                    noticePeriodStatusLine = $scope.genderAddress + " can join in <B>" + $scope.candidateDetailsSummary.Time_To_Join + "</B>";
                }*/else {
                    noticePeriodStatusLine = $scope.genderAddress + " can join in <B>" + $scope.candidateDetailsSummary.Time_To_Join + "</B> days.";
                }
                $scope.noticePeriodStatusLinehtml =  $sce.trustAsHtml(noticePeriodStatusLine);

                var summaryComments = $scope.firstLine + $scope.secondLine + salaryLine + $scope.employmentStatusLine + noticePeriodStatusLine;
                $scope.summaryCommentshtml =  $sce.trustAsHtml(summaryComments);


        }

            $scope.$apply(function () {
                $scope.candidateSummaryTableLoadingStatus = masterResultStatus + interactionResultStatus;
                $scope.candidateSummaryTable = true;
                //console.log("candidateSummaryTable" + $scope.candidateSummaryTable);

            });
        });

        console.log("Get Candidate Summary from Interaction Master");
        //console.log("Selected candidateID -> " + $scope.candidateID);
        var queryUrl = getCandidateInteractionForCandidateIDQueryURL($scope.candidateID,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);
        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            //console.log(JSON.stringify(parsedJSONData));
            var formattedData = formatResult(parsedJSONData);
            console.log("formattedData" + formattedData);
            var interactionResults = formattedData.ResultSet;
            if (typeof interactionResults == "undefined") {
                console.log(" No Interactions Found");
                $scope.firstAssessmentComments = getInteractionComments(interactionResults,'First Assessment Passed (Internal)');
                $scope.secondAssessmentComments = getInteractionComments(interactionResults,'Second Assessment Passed (Internal)');
                $scope.thirdAssessmentComments = getInteractionComments(interactionResults,'Third Assessment Passed (Internal)');
            } else {
                console.log("interactionResults" + interactionResults);
                //console.log(JSON.stringify(interactionResults));
                //console.log($scope.candidateInteractionDetailsSummary.Technical_rating);
                $scope.firstAssessmentComments = getInteractionComments(interactionResults,'First Assessment Passed (Internal)');
                $scope.secondAssessmentComments = getInteractionComments(interactionResults,'Second Assessment Passed (Internal)');
                $scope.thirdAssessmentComments = getInteractionComments(interactionResults,'Third Assessment Passed (Internal)');

            }

            $scope.$apply(function () {
                $scope.candidateSummaryTableLoadingStatus = masterResultStatus + interactionResultStatus;
                $scope.candidateInteractionSummary = true;
                //console.log("candidateInteractionSummary" + $scope.candidateInteractionSummary);
            });
        });

    };
}