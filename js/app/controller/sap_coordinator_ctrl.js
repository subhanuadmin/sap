/**
 * Created by snehita on 12/4/2015.
 */

function SAPCoordinatorCtrl($scope,$rootScope,$filter,$modal,$window) {
    console.log('SAPCoordinatorReportCtrl');

    $scope.getResponsibility = function () {
        console.log("Load Responsibility Status");
        $window.open("https://drive.google.com/open?id=1EW6bCM97f6nOkCLAMNIYHsitMhrRWDmuid7b3EIsT3E", '_target');

    };
    $scope.getCandidateMaster = function () {
        console.log("Load Candidate Master Status");
        $window.open("https://drive.google.com/open?id=1C3OL7wowC7zeIwCY9qIKpbLixYx4gf3yMjwsqRnG5_4", '_target');

    };

    $scope.getErrorReportStatus = function () {
        console.log("Load ErrorReport Status");
        $window.open("https://drive.google.com/open?id=1DaHdNX6871UhSCpQMDbfaWh4SFTar42_KU6AE1lJ-WA", '_target');

    };
    $scope.getJoiningDiscrepancy = function () {
        console.log("Load Joining Discrepancy Status");
        $window.open("https://drive.google.com/open?id=1oqIl0msbkLjCeRS0nMHjh1N5Wi6gHmJtT-omYntdYiQ", '_target');

    };
}