/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */


function ActivityMasterCtrl($scope,$window,$rootScope,notify) {
    console.log('ActivityMasterCtrl');

    $scope.loadActivityMasterForm = function () {
        console.log("Load Activity Tracker For");
        console.log($rootScope.trackerURL);
        if ($rootScope.loggedInUser == null){
            notify({
                message:'Please set user',
                classes: 'alert-danger'
            })
        } else {
            //$window.open("https://docs.google.com/a/subhanu.com/spreadsheets/d/1Q3iO1kY-W1-qrPScVGkvq7BiOSRORR8OXa-pWefyvwU/edit#gid=601975560", '_target');
            $window.open($rootScope.trackerURL, '_target');
        }

        console.log("After loadActivityMasterForm");
    };
}