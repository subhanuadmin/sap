/**
 * Created by Rajesh_Krishnan1 on 2/24/2015.
 */

function RequirementSummaryCtrl($scope,$rootScope,$filter,$modal,$window) {
    console.log('RequirementMasterCtrl');
    $scope.requirementSummaryTable = false;
    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.toNum =  function (CountInput) {
        return Number(CountInput);
    };

    $scope.$watch('requirementSummaryTable', function() {
        // alert('hey, myVar has changed!');
    });

    //Loading Dropdown on view
    $scope.initRequirementSummary = function () {
        console.log("Init Interview Schedule");
        var masterResults = {
            "Open": [{
                "Client": "Big Basket(BBC)",
                "Role": "Python Developer",
                "RequirementID": "BBC-JNR-PYT",
                "No_of_positions": 1,
                "Req_Open_Date": "",
                "Status": "Offered",
                "Last_Date_Checked_with_Client": "",
                "Posn_CV_Ratio": "",
                "CV's_Required": 0,
                "Min_Salary": 900000,
                "Max_Salary": 1100000,
                "Avg_Salary": 1000000,
                "Subhanu_Svc_Chg": 0.0833,
                "Invoice_%": 0.0833,
                "Service_Tax": 0.1236,
                "Requirement_Sequence": "",
                "Position_Sequence": "",
                "Priority": ""
            }]
        };
        $scope.masterList = masterResults.Open;
        console.log($rootScope.activeRequirements.length);
        if ($rootScope.activeRequirements != null && $rootScope.activeRequirements.length == 0 ) {
            console.log("Getting Requirements ");
            var queryUrl = getRequirementsMasterDocsQueryURL($rootScope.root_stored_token);
            console.log("queryUrl -> " + queryUrl);
            var query = new google.visualization.Query(queryUrl);

            // Send the query with a callback function.
            query.send(function handleQueryResponse(response) {
                if (response.isError()) {
                    alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                    return;
                }
                data = response.getDataTable();
                var jsonData = data.toJSON();
                var parsedJSONData = JSON.parse(jsonData);
                var formattedData = formatResult(parsedJSONData);
                $rootScope.activeRequirements = formattedData.ResultSet;
                console.log("Loaded to Cache");
                console.log($rootScope.activeRequirements);
                $scope.masterList = $rootScope.activeRequirements;
            });
        }else {
            console.log("Picking from cache");
            $scope.masterList = $rootScope.activeRequirements;
            //console.log(JSON.stringify($scope.masterList));
        }
    };

    $scope.getRequirementSummary = function () {
        $scope.requirementSummaryLoadingStatus = "Loading";
        console.log("Get Requirement");
        if(typeof $scope.requirement.Requirement_ID == "undefined"){
            $scope.requirementSummaryLoadingStatus = "Please select Requirement ID";
            return;
        }

        var startDateWithSlash = $filter('date')($scope.a, "yyyy/MM/dd");
        var endDateWithSlash = $filter('date')($scope.b, "yyyy/MM/dd");
        var startDateWithHyphen = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDateWithHyphen = $filter('date')($scope.b, "yyyy-MM-dd");
        var docsIncluded = true;

        console.log("Selected requirementID -> " + $scope.requirement.Requirement_ID);
        console.log("Selected - Start Date -> " + startDateWithSlash );
        console.log("Selected - End Date -> " + endDateWithSlash);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);
        console.log("Selected - DocsOnly -> " + $scope.docsOnly);

        if (typeof $scope.docsOnly == "undefined") {
            console.log("Docs Not Included");
            var docsIncluded = false;
        }
        console.log("Selected - docsIncluded -> " + docsIncluded);
        $scope.requirementSummaryTable = false;
        var queryUrl = getDocumentsQueryURL($scope.requirement.Requirement_ID,'All',$scope.talentSpecialist,startDateWithHyphen,endDateWithHyphen,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
           // console.log(JSON.stringify(formattedData));
            var results = formattedData.ResultSet;
            if (typeof results == "undefined") {
                console.log("No Data Found");
                requirementSummaryLoadingStatus = "No Data Found ";
                $scope.activitySummaryCount = getRequirementSummaryCount(results);
                console.log(JSON.stringify($scope.activitySummaryCount));
            } else {
                console.log("Results fetched successfully from Activity Master");
                $scope.documents = results;
                //console.log(results);
                $scope.activitySummaryCount = getRequirementSummaryCount(results);
              //  console.log(JSON.stringify($scope.activitySummaryCount));
           }

            $scope.$apply(function () {
                $scope.requirementSummaryLoadingStatus = "";
                $scope.requirementSummaryTable = true;
                //console.log("requirementSummaryTable" + $scope.requirementSummaryTable);

            });
        });

        var queryUrl = getAllInteractionDetailsQueryURL($scope.requirement.Requirement_ID,'',$scope.talentSpecialist,startDateWithHyphen,endDateWithHyphen,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            //console.log(JSON.stringify(formattedData));
            var results = formattedData.ResultSet;
            if (typeof results == "undefined") {
                console.log("No Data Found");
                $scope.requirementSummaryLoadingStatus = "No Data Found";
                $scope.results = {};
                $scope.candidateSummary = {};
                //$scope.candidateSummary = getCandidateStatusSummary($scope.results);
                $rootScope.candidateSummary =  $scope.candidateSummary;
                $scope.candidateInteractionEffort = 0;
            } else {
                console.log("Results fetched successfully from Activity Master");
                //console.log(results);
                $scope.results = results;
                $scope.candidateStatusResults = getCandidateStatusSummary(results);
                $scope.candidateSummary = $scope.candidateStatusResults.candidateStatusSummaries;
                $scope.candidateInteractionEffort = $scope.candidateStatusResults.Total_Expended_Effort;
                $rootScope.candidateSummary =  $scope.candidateSummary;
                //console.log(JSON.stringify($scope.candidateSummary));
                //console.log("After Update");
               // console.log(results);
            }

            $scope.$apply(function () {
                $scope.requirementSummaryLoadingStatus = "";
                $scope.requirementSummaryTable = true;
                //console.log("requirementSummaryTable" + $scope.requirementSummaryTable);

            });
        });


    };

    $scope.openInteractionDetails = function (size, interactionStatus) {
        var modalInstance = $modal.open({
            templateUrl: 'views/dashboard/interaction_details.html',
            size: size,
            controller: ModalInteractionDetailsCtrl,
            resolve: {
                modalResults: function () {
                    console.log("Modal Status" + interactionStatus);
                    $scope.statusResults = getFilteredResultsForStatus($scope.results,interactionStatus);
                    console.log("$scope.statusResult" + $scope.statusResults.length);
                    $rootScope.statusResults =  $scope.statusResults ;
                    $rootScope.selectedStatus = interactionStatus;
                    console.log("$rootScope.statusResults" + $rootScope.statusResults.length);
                    return $scope.statusResults;
                }
            }
        });
    };

}