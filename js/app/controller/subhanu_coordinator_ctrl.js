/**
 * Created by snehita on 12/4/2015.
 */


function SubhanuCoordinatorCtrl($scope,$rootScope,$filter,$modal,$window) {
    console.log('SubhanuCoordinatorReportCtrl');

    $scope.getResponsibility = function () {
        console.log("Load Responsibility Status");
        $window.open("https://drive.google.com/open?id=1-3l3RnMOUaX4Y-sFlrS46Ftqjqj5uMXz12av4jlsjMw", '_target');

    };

    $scope.getFBAlumniReport = function () {
        console.log("FB Alumni Report");
        $window.open("https://drive.google.com/open?id=1q3SElsAinZqCnTE-phziI8YuFP1r-mnrYgjXsfkCPqw", '_target');

    };

    $scope.getSubhanuGrowth = function () {
        console.log("Load Subhanu Growth Status");
        $window.open("https://drive.google.com/open?id=10lwZywZ7DqM7FpSs7YunVzshhIM8_TPO7FVXCjULqK4", '_target');

    };

}