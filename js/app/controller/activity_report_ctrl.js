/**
 * Created by snehita on 4/6/2016.
 */
function ActivityReportCtrl($scope, $location, $rootScope, $window, $filter) {
    console.log("ActivityReportCtrl");
    $scope.activateReportTable = false;

    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);



    $scope.activities =[{
        "Timestamp": "2016-01-28T18:30:00.000Z",
        "Username": "Snehitha",
        "Activity_Category": "Development",
        "Requirement_ID":"inrc-crc111",
        "Time_Spent": "Better taking",
        "Activity_Comments": "Good"
    }, {
        "Timestamp": "2016-01-28T18:30:00.000Z",
        "Username": "Snehitha",
        "Activity_Category": "Development",
        "Requirement_ID":"inrc-crc111",
        "Time_Spent": "Better taking",
        "Activity_Comments": "Good"
    }];




    $scope.$watch('activateReportTable', function () {
        // alert('hey, myVar has changed!');
    });
    $scope.getActivityReportDetails = function () {
        $scope.activityReportLoadingStatus = "Loading";
        console.log("loadActivityReport");


        var startDateWithSlash = $filter('date')($scope.a, "yyyy/MM/dd");
        var endDateWithSlash = $filter('date')($scope.b, "yyyy/MM/dd");
        var startDateWithHyphen = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDateWithHyphen = $filter('date')($scope.b, "yyyy-MM-dd");

        console.log("Selected - Start Date -> " + startDateWithHyphen );
        console.log("Selected - End Date -> " + endDateWithHyphen);
        console.log("Selected - Talent Specialist -> " + $scope.talentSpecialist);


        $scope.activities =[{
            "Timestamp": "2016-01-28T18:30:00.000Z",
            "Username": "Snehitha",
            "Activity_Category": "Development",
            "Requirement_ID":"inrc-crc111",
            "Time_Spent": "Better taking",
            "Activity_Comments": "Good"
        }, {
            "Timestamp": "2016-01-28T18:30:00.000Z",
            "Username": "Snehitha",
            "Activity_Category": "Development",
            "Requirement_ID":"inrc-crc111",
            "Time_Spent": "Better taking",
            "Activity_Comments": "Good"

        }];

        var queryUrl = getActivityReportURL(startDateWithHyphen,endDateWithHyphen,$scope.talentSpecialist,$rootScope.root_stored_token);
        //var queryUrl = "";
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);



        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.activities = formattedData.ResultSet;
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.activityReportLoadingStatus = "";
                $scope.activateReportTable = true;
            });
        });
    };
}
