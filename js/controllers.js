/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

function notifyCtrl($scope, notify) {
    $scope.msg = 'Hello! This is a sample message!';
    $scope.demo = function () {
        notify({
            message: $scope.msg,
            classes: $scope.classes,
            templateUrl: $scope.template,
        });
    };
    $scope.closeAll = function () {
        notify.closeAll();
    };

    $scope.inspiniaTemplate = 'views/common/notify.html';
    $scope.inspiniaDemo1 = function(){
        notify({ message: 'Info - This is a Inspinia info notification', classes: 'alert-info', templateUrl: $scope.inspiniaTemplate});
    }
    $scope.inspiniaDemo2 = function(){
        notify({ message: 'Success - This is a Inspinia success notification', classes: 'alert-success', templateUrl: $scope.inspiniaTemplate});
    }
    $scope.inspiniaDemo3 = function(){
        notify({ message: 'Warning - This is a Inspinia warning notification', classes: 'alert-warning', templateUrl: $scope.inspiniaTemplate});
    }
    $scope.inspiniaDemo4 = function(){
        notify({ message: 'Danger - This is a Inspinia danger notification', classes: 'alert-danger', templateUrl: $scope.inspiniaTemplate});
    }
}

angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)
    .controller('notifyCtrl', notifyCtrl)
    .controller('CandidateMasterCtrl', CandidateMasterCtrl)
    .controller('CandidateInteractionCtrl', CandidateInteractionCtrl)
    .controller('RequirementMasterCtrl', RequirementMasterCtrl)
    .controller('DailyMetricsMasterCtrl', DailyMetricsMasterCtrl)
    .controller('WorkDayMasterCtrl', WorkDayMasterCtrl)
    .controller('ActivityMasterCtrl', ActivityMasterCtrl)
    .controller('InterviewScheduleCtrl', InterviewScheduleCtrl)
    .controller('JoiningReportCtrl', JoiningReportCtrl)
    .controller('FeedbackDetailsCtrl', FeedbackDetailsCtrl)
    .controller('EmailCandidateSummaryCtrl', EmailCandidateSummaryCtrl)
    .controller('RequirementSummaryCtrl', RequirementSummaryCtrl)
    .controller('JoiningCandidateReportCtrl',JoiningCandidateReportCtrl)
    .controller('OfferAcceptedCtrl',OfferAcceptedCtrl)
    .controller('ModalInteractionDetailsCtrl',ModalInteractionDetailsCtrl)
    .controller('StatusReportCtrl',StatusReportCtrl)
    .controller('SubhanuInfoCtrl',SubhanuInfoCtrl)
    .controller('CandidateCoordinatorCtrl',CandidateCoordinatorCtrl)
    .controller('JobBoardCoordinatorCtrl',JobBoardCoordinatorCtrl)
    .controller('AssessmentCoordinatorCtrl',AssessmentCoordinatorCtrl)
    .controller('JoiningCoordinatorCtrl',JoiningCoordinatorCtrl)
    .controller('SubhanuCoordinatorCtrl',SubhanuCoordinatorCtrl)
    .controller('SAPCoordinatorCtrl',SAPCoordinatorCtrl)
    .controller('ClientCoordinatorCtrl',ClientCoordinatorCtrl)
    .controller('LeadCtrl',LeadCtrl)
    .controller('MeetingReportCtrl',MeetingReportCtrl)
    .controller('ActivityReportCtrl',ActivityReportCtrl)
