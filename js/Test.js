/**
 * Created by Rajesh_Krishnan1 on 3/4/2015.
 */
/* Send Google Form by Email v2.1 */
/* For customization, contact the developer at amit@labnol.org */
/* Tutorial: http://www.labnol.org/?p=20884 */
function Initialize() {
    var triggers = ScriptApp.getProjectTriggers();

    for (var i in triggers) {
        ScriptApp.deleteTrigger(triggers[i]);
    }
    ScriptApp.newTrigger("SendGoogleForm")
        .forSpreadsheet(SpreadsheetApp.getActiveSpreadsheet())
        .onFormSubmit()
        .create();
}
function SendGoogleForm(e) {
    try {
        // You may replace this with another email address
        var email = Session.getActiveUser().getEmail();
        // Optional but change the following variable
        // to have a custom subject for Google Form email notifications
        var subject = "Google Docs Form Submitted";
        var s = SpreadsheetApp.getActiveSheet();
        var columns = s.getRange(1, 1, 1, s.getLastColumn()).getValues()[0];
        var message = "";
        // Only include form fields that are not blank
        for (var keys in columns) {
            var key = columns[keys];
            if (e.namedValues[key] && (e.namedValues[key] != "")) {
                message += key + ' :: ' + e.namedValues[key] + "\n\n";
            }
        }
        // This is the MailApp service of Google Apps Script
        // that sends the email. You can also use GmailApp for HTML Mail.
        MailApp.sendEmail(email, subject, message);
    } catch (e) {
        Logger.log(e.toString());
    }
}