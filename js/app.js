/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */
(function () {
    angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'splitFilter',
        'specialCharacterFilter',
        'CommonServices',
        'CandidateMasterServices',
        'datatables',
        'dateConverter',
    ])
})();

